var searchData=
[
  ['daradbcontext_10',['DaraDBContext',['../class_dara_zrt_1_1_data_1_1_dara_d_b_context.html',1,'DaraZrt.Data.DaraDBContext'],['../class_dara_zrt_1_1_data_1_1_dara_d_b_context.html#a3d61d5f069aa9193c68b432ccc9ae2d6',1,'DaraZrt.Data.DaraDBContext.DaraDBContext()'],['../class_dara_zrt_1_1_data_1_1_dara_d_b_context.html#a4dd972181f268fc65f53083828d457cf',1,'DaraZrt.Data.DaraDBContext.DaraDBContext(DbContextOptions&lt; DaraDBContext &gt; options)']]],
  ['darairoda_11',['DaraIroda',['../class_dara_zrt_1_1_logic_1_1_dara_iroda.html',1,'DaraZrt.Logic.DaraIroda'],['../class_dara_zrt_1_1_logic_1_1_dara_iroda.html#ae34eb86fab8a5170f27167ab59a5b617',1,'DaraZrt.Logic.DaraIroda.DaraIroda()']]],
  ['darazrt_12',['DaraZrt',['../namespace_dara_zrt.html',1,'']]],
  ['data_13',['Data',['../namespace_dara_zrt_1_1_data.html',1,'DaraZrt']]],
  ['datum_14',['Datum',['../class_dara_zrt_1_1_data_1_1_models_1_1_utfoglalas.html#aba3e31fced57e655f1c4aa3e47963915',1,'DaraZrt::Data::Models::Utfoglalas']]],
  ['deleteinditottbuszok_15',['DeleteInditottBuszok',['../class_dara_zrt_1_1_logic_1_1_dara_iroda.html#a4aa691972c7cf6b72d2a21499534c5be',1,'DaraZrt.Logic.DaraIroda.DeleteInditottBuszok()'],['../interface_dara_zrt_1_1_logic_1_1_i_dara_iroda.html#a7aafca6d5fdda5865154e7cb2cf36364',1,'DaraZrt.Logic.IDaraIroda.DeleteInditottBuszok()']]],
  ['deleteszallas_16',['DeleteSzallas',['../class_dara_zrt_1_1_logic_1_1_dara_iroda.html#aa40e9ad2fd41addb60d2ebe95a927b3e',1,'DaraZrt.Logic.DaraIroda.DeleteSzallas()'],['../interface_dara_zrt_1_1_logic_1_1_i_dara_iroda.html#ad80b46d6fd8e05ab901afb6b9fcbfa07',1,'DaraZrt.Logic.IDaraIroda.DeleteSzallas()']]],
  ['deleteutas_17',['DeleteUtas',['../class_dara_zrt_1_1_logic_1_1_dara_iroda.html#af2626b1f4dfbd60bf3a74d6078ca06ec',1,'DaraZrt.Logic.DaraIroda.DeleteUtas()'],['../interface_dara_zrt_1_1_logic_1_1_i_dara_iroda.html#ab34d6836d4e4620ce00b5eb7e5c37979',1,'DaraZrt.Logic.IDaraIroda.DeleteUtas()']]],
  ['deleteutazas_18',['DeleteUtazas',['../class_dara_zrt_1_1_logic_1_1_dara_iroda.html#a65a4b98f8b0923fc7617cad4321435eb',1,'DaraZrt.Logic.DaraIroda.DeleteUtazas()'],['../interface_dara_zrt_1_1_logic_1_1_i_dara_iroda.html#ad37ce13f0bfc2f71db89100f5a4b11b5',1,'DaraZrt.Logic.IDaraIroda.DeleteUtazas()']]],
  ['deleteutfoglalas_19',['DeleteUtfoglalas',['../class_dara_zrt_1_1_logic_1_1_dara_iroda.html#ab5453359ad5dce8d377cdda4b9833ab1',1,'DaraZrt.Logic.DaraIroda.DeleteUtfoglalas()'],['../interface_dara_zrt_1_1_logic_1_1_i_dara_iroda.html#a5206171b58d194f9357baa66bfebb30b',1,'DaraZrt.Logic.IDaraIroda.DeleteUtfoglalas()']]],
  ['logic_20',['Logic',['../namespace_dara_zrt_1_1_logic.html',1,'DaraZrt']]],
  ['models_21',['Models',['../namespace_dara_zrt_1_1_data_1_1_models.html',1,'DaraZrt.Data.Models'],['../namespace_dara_zrt_1_1_logic_1_1_models.html',1,'DaraZrt.Logic.Models']]],
  ['repository_22',['Repository',['../namespace_dara_zrt_1_1_repository.html',1,'DaraZrt']]],
  ['tests_23',['Tests',['../namespace_dara_zrt_1_1_logic_1_1_tests.html',1,'DaraZrt::Logic']]]
];
