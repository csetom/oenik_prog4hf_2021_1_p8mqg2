var searchData=
[
  ['idarairoda_96',['IDaraIroda',['../interface_dara_zrt_1_1_logic_1_1_i_dara_iroda.html',1,'DaraZrt::Logic']]],
  ['inditottbuszok_97',['InditottBuszok',['../class_dara_zrt_1_1_data_1_1_models_1_1_inditott_buszok.html',1,'DaraZrt::Data::Models']]],
  ['inditottbuszokrepository_98',['InditottBuszokRepository',['../class_dara_zrt_1_1_repository_1_1_inditott_buszok_repository.html',1,'DaraZrt::Repository']]],
  ['irepository_99',['IRepository',['../interface_dara_zrt_1_1_repository_1_1_i_repository.html',1,'DaraZrt::Repository']]],
  ['irepository_3c_20darazrt_3a_3adata_3a_3amodels_3a_3ainditottbuszok_20_3e_100',['IRepository&lt; DaraZrt::Data::Models::InditottBuszok &gt;',['../interface_dara_zrt_1_1_repository_1_1_i_repository.html',1,'DaraZrt::Repository']]],
  ['irepository_3c_20darazrt_3a_3adata_3a_3amodels_3a_3aszallas_20_3e_101',['IRepository&lt; DaraZrt::Data::Models::Szallas &gt;',['../interface_dara_zrt_1_1_repository_1_1_i_repository.html',1,'DaraZrt::Repository']]],
  ['irepository_3c_20darazrt_3a_3adata_3a_3amodels_3a_3autas_20_3e_102',['IRepository&lt; DaraZrt::Data::Models::Utas &gt;',['../interface_dara_zrt_1_1_repository_1_1_i_repository.html',1,'DaraZrt::Repository']]],
  ['irepository_3c_20darazrt_3a_3adata_3a_3amodels_3a_3autazas_20_3e_103',['IRepository&lt; DaraZrt::Data::Models::Utazas &gt;',['../interface_dara_zrt_1_1_repository_1_1_i_repository.html',1,'DaraZrt::Repository']]],
  ['irepository_3c_20darazrt_3a_3adata_3a_3amodels_3a_3autfoglalas_20_3e_104',['IRepository&lt; DaraZrt::Data::Models::Utfoglalas &gt;',['../interface_dara_zrt_1_1_repository_1_1_i_repository.html',1,'DaraZrt::Repository']]]
];
