﻿// <copyright file="DaraStaticTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DaraZrt.Data.Models;
    using DaraZrt.Logic.Models;
    using DaraZrt.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// DaraStaticTest.
    /// </summary>
    [TestFixture]
    public class DaraStaticTest
    {
        /// <summary>
        /// TestGetCheapBooking.
        /// </summary>
        [Test]
        public void TestGetCheapBooking()
        {
            Mock<IPersonsRepository> mockedPersoneRepo = new Mock<IPersonsRepository>(
                MockBehavior.Loose);
            Mock<IBusesRepository> mockedBusesRepo = new Mock<IBusesRepository>(
                MockBehavior.Loose);
            Mock<IBookingsRepository> mockedBookingRepo = new Mock<IBookingsRepository>(
                MockBehavior.Loose);
            Mock<ITravelRepository> mockedTravelRepo = new Mock<ITravelRepository>(
                MockBehavior.Loose);
            Mock<ILodgingsRepository> mockedLodgingsRepo = new Mock<ILodgingsRepository>(
                MockBehavior.Loose);
            List<Bookings> bookings = new List<Bookings>
            {
                        new Bookings() { Id = 1, BusId = 1, PersonId = 2, Date = DateTime.Now, TravelId = 1, LodgingsId = 1, Seats = "42" },
                        new Bookings() { Id = 2, BusId = 2, PersonId = 2, Date = DateTime.Now, TravelId = 2, LodgingsId = 5, Seats = "22" },
                        new Bookings() { Id = 3, BusId = 3, PersonId = 1, Date = DateTime.Now, TravelId = 1, LodgingsId = 2, Seats = "39" },
                        new Bookings() { Id = 4, BusId = 3, PersonId = 3, Date = DateTime.Now, TravelId = 2, LodgingsId = 6, Seats = "11" },
            };
            mockedBookingRepo.Setup(repo => repo.GetAll()).Returns(bookings.AsQueryable());

            List<Lodgings> lodgings = new List<Lodgings>
            {
                        new Lodgings() { Id = 1, Price = 20000, Quality = "Alap", Hotel = "Furaszalloda", RoomNumber = "G2", Space = 2, },
                        new Lodgings() { Id = 2, Price = 40000, Quality = "Minosegi", Hotel = "Furaszalloda", RoomNumber = "G3", Space = 2, },
                        new Lodgings() { Id = 3, Price = 60000, Quality = "Kiralyi", Hotel = "Furaszalloda", RoomNumber = "G4", Space = 2, },
                        new Lodgings() { Id = 4, Price = 10000, Quality = "Alap", Hotel = "Kecske lábszár", RoomNumber = "A1", Space = 3, },
                        new Lodgings() { Id = 5, Price = 20000, Quality = "Minosegi", Hotel = "Kecske lábszár", RoomNumber = "A2", Space = 1, },
                        new Lodgings() { Id = 6, Price = 30000, Quality = "Alap", Hotel = "Lépcsős Pincelakók", RoomNumber = "A113", Space = 6, },
            };
            mockedLodgingsRepo.Setup(repo => repo.GetAll()).Returns(lodgings.AsQueryable());

            List<Travel> travels = new List<Travel>
            {
                new Travel() { Id = 1, Price = 20000, Starts = DateTime.ParseExact("2023.06.09", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2023.06.16", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" },
                new Travel() { Id = 2, Price = 300000, Starts = DateTime.ParseExact("2022.09.02T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2022.09.09T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" },
            };
            mockedTravelRepo.Setup(repo => repo.GetAll()).Returns(travels.AsQueryable());
            DaraStatistics daraStatistics = new DaraStatistics(mockedBookingRepo.Object, mockedPersoneRepo.Object, mockedBusesRepo.Object, mockedTravelRepo.Object, mockedLodgingsRepo.Object);
            var result = daraStatistics.GetCheapBooking();
            List<CheapBooking> cheapBookings = new List<CheapBooking>
            {
                new CheapBooking() { Id = 1, Price = 40000, City = "Dubaj", Country = "Egyesült Arab Emírségek", Starts = DateTime.ParseExact("2023.06.09", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2023.06.16", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Hotel = "Furaszalloda", Quality = "Alap" },
            };
            Assert.That(result, Is.EquivalentTo(cheapBookings));
            mockedTravelRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedTravelRepo.Verify(repo => repo.GetAll(), Times.Once);
            mockedBookingRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedBookingRepo.Verify(repo => repo.GetAll(), Times.Once);
            mockedLodgingsRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedLodgingsRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test GetBookableBusses.
        /// </summary>
        [Test]
        public void TestGetBookableBusses()
        {
            Mock<IPersonsRepository> mockedPersoneRepo = new Mock<IPersonsRepository>(
                 MockBehavior.Loose);
            Mock<IBusesRepository> mockedBusesRepo = new Mock<IBusesRepository>(
                MockBehavior.Loose);
            Mock<IBookingsRepository> mockedBookingRepo = new Mock<IBookingsRepository>(
                MockBehavior.Loose);
            Mock<ITravelRepository> mockedTravelRepo = new Mock<ITravelRepository>(
                MockBehavior.Loose);
            Mock<ILodgingsRepository> mockedLodgingsRepo = new Mock<ILodgingsRepository>(
                MockBehavior.Loose);
            List<Bookings> bookings = new List<Bookings>
            {
                        new Bookings() { Id = 1, BusId = 1, PersonId = 2, Date = DateTime.Now, TravelId = 1, LodgingsId = 1, Seats = "42" },
                        new Bookings() { Id = 2, BusId = 2, PersonId = 2, Date = DateTime.Now, TravelId = 2, LodgingsId = 5, Seats = "22" },
                        new Bookings() { Id = 3, BusId = 3, PersonId = 1, Date = DateTime.Now, TravelId = 1, LodgingsId = 2, Seats = "39" },
                        new Bookings() { Id = 4, BusId = 3, PersonId = 3, Date = DateTime.Now, TravelId = 2, LodgingsId = 6, Seats = "11" },
            };
            mockedBookingRepo.Setup(repo => repo.GetAll()).Returns(bookings.AsQueryable());
            List<Buses> buses = new List<Buses>
            {
                new Buses() { Id = 1, Capacity = 50, LicencePlate = "ABC-123", Departures = DateTime.ParseExact("2023.06.08T08:10", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2023.06.09T21:33", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 5 },
                new Buses() { Id = 2, Capacity = 20, LicencePlate = "GEG-001", Departures = DateTime.ParseExact("2022.09.08T10:11", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2022.09.09T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 7 },
                new Buses() { Id = 3, Capacity = 100, LicencePlate = "FIN-626", Departures = DateTime.ParseExact("2021.12.01T05:05", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2021.12.01T22:09", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 9 },
            };
            mockedBusesRepo.Setup(repo => repo.GetAll()).Returns(buses.AsQueryable());
            DaraStatistics daraStatistics = new DaraStatistics(mockedBookingRepo.Object, mockedPersoneRepo.Object, mockedBusesRepo.Object, mockedTravelRepo.Object, mockedLodgingsRepo.Object);
            var result = daraStatistics.GetBookableBusses();
            /*
                        1 ABC - 123 : 49 / 50
                        2 GEG - 001 : 19 / 20
                        3 FIN - 626 : 98 / 100
            */
            List<BookableBusses> bookableBusses = new List<BookableBusses>
            {
                new BookableBusses() { Id = 1, LicencePlate = "ABC-123", NumberOfFreeSeats = 49, NumberOfSeats = 50 },
                new BookableBusses() { Id = 2, LicencePlate = "GEG-001", NumberOfFreeSeats = 19, NumberOfSeats = 20 },
                new BookableBusses() { Id = 3, LicencePlate = "FIN-626", NumberOfFreeSeats = 98, NumberOfSeats = 100 },
            };
            Assert.That(result, Is.EquivalentTo(bookableBusses));
            mockedBookingRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedBookingRepo.Verify(repo => repo.GetAll(), Times.Once);
            mockedBusesRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedBusesRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test GetSummerPeople.
        /// </summary>
        [Test]
        public void TestGetSummerPeople()
        {
            Mock<IPersonsRepository> mockedPersoneRepo = new Mock<IPersonsRepository>(
                MockBehavior.Loose);
            Mock<IBusesRepository> mockedBusesRepo = new Mock<IBusesRepository>(
                MockBehavior.Loose);
            Mock<IBookingsRepository> mockedBookingRepo = new Mock<IBookingsRepository>(
                MockBehavior.Loose);
            Mock<ITravelRepository> mockedTravelRepo = new Mock<ITravelRepository>(
                MockBehavior.Loose);
            Mock<ILodgingsRepository> mockedLodgingsRepo = new Mock<ILodgingsRepository>(
                MockBehavior.Loose);
            List<Bookings> bookings = new List<Bookings>
            {
                        new Bookings() { Id = 1, BusId = 1, PersonId = 2, Date = DateTime.Now, TravelId = 1, LodgingsId = 1, Seats = "42" },
                        new Bookings() { Id = 2, BusId = 2, PersonId = 2, Date = DateTime.Now, TravelId = 2, LodgingsId = 5, Seats = "22" },
                        new Bookings() { Id = 3, BusId = 3, PersonId = 1, Date = DateTime.Now, TravelId = 1, LodgingsId = 2, Seats = "39" },
                        new Bookings() { Id = 4, BusId = 3, PersonId = 3, Date = DateTime.Now, TravelId = 2, LodgingsId = 6, Seats = "11" },
            };
            mockedBookingRepo.Setup(repo => repo.GetAll()).Returns(bookings.AsQueryable());

            List<Persons> persons = new List<Persons>()
            {
                        new Persons() { Id = 1, Email = "Karcsi@Karcsilagzi.hu", FirstName = "Károly", LastName = "Buli", PhoneNumber = "+363033333333", Adress = "1111 Budapest XI. Balaton utca 2" },
                        new Persons() { Id = 2, Email = "Marika@mzperx.hu", FirstName = "Mariann", LastName = "Soos-Ferenczné Nagy", PhoneNumber = "+320000351000", Adress = "8174 Balatonkenese Simon István utca 11" },
                        new Persons() { Id = 3, Email = "Zoldlevelibeka@narancsmail.hu", FirstName = "Gergely", LastName = "Csíkos", PhoneNumber = "+310011110", Adress = "8172 Balatonakarattya  Zöld-ház út 222" },
            };
            List<Travel> travels = new List<Travel>
            {
                new Travel() { Id = 1, Price = 20000, Starts = DateTime.ParseExact("2023.06.09", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2023.06.16", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" },
                new Travel() { Id = 2, Price = 300000, Starts = DateTime.ParseExact("2022.09.02T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2022.09.09T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" },
            };
            mockedTravelRepo.Setup(repo => repo.GetAll()).Returns(travels.AsQueryable());

            mockedPersoneRepo.Setup(repo => repo.GetAll()).Returns(persons.AsQueryable());
            DaraStatistics daraStatistics = new DaraStatistics(mockedBookingRepo.Object, mockedPersoneRepo.Object, mockedBusesRepo.Object, mockedTravelRepo.Object, mockedLodgingsRepo.Object);
            var result = daraStatistics.GetSummerPeople();
            List<SummerPeople> summerPeoples = new List<SummerPeople>()
            {
                new SummerPeople() { Id = 1, FirstName = "Károly", LastName = "Buli", City = "Dubaj", Start = DateTime.ParseExact("2023.06.09", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), End = DateTime.ParseExact("2023.06.16", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture) },
                new SummerPeople() { Id = 2, FirstName = "Mariann", LastName = "Soos-Ferenczné Nagy", City = "Dubaj", Start = DateTime.ParseExact("2023.06.09", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), End = DateTime.ParseExact("2023.06.16", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture) },
            };
            Assert.That(result, Is.EquivalentTo(summerPeoples));
            mockedPersoneRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedPersoneRepo.Verify(repo => repo.GetAll(), Times.Once);
            mockedBookingRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedBookingRepo.Verify(repo => repo.GetAll(), Times.Once);
            mockedTravelRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedTravelRepo.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
