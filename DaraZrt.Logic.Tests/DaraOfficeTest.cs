﻿// <copyright file="DaraOfficeTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DaraZrt.Data.Models;
    using DaraZrt.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Dara Office Test.
    /// </summary>
    [TestFixture]
    public class DaraOfficeTest
    {
        /// <summary>
        /// TestGetAll.
        /// </summary>
        [Test]
        public void TestGetAll()
        {
            Mock<IPersonsRepository> mockedPersoneRepo = new Mock<IPersonsRepository>(
                MockBehavior.Loose);
            Mock<IBusesRepository> mockedBusesRepo = new Mock<IBusesRepository>(
                MockBehavior.Loose);
            Mock<IBookingsRepository> mockedBookingRepo = new Mock<IBookingsRepository>(
                MockBehavior.Loose);
            Mock<ITravelRepository> mockedTravelRepo = new Mock<ITravelRepository>(
                MockBehavior.Loose);
            Mock<ILodgingsRepository> mockedLodgingsRepo = new Mock<ILodgingsRepository>(
                MockBehavior.Loose);
            List<Lodgings> lodgings = new List<Lodgings>
            {
                        new Lodgings() { Id = 1, Price = 20000, Quality = "Alap", Hotel = "Furaszalloda", RoomNumber = "G2", Space = 2, },
                        new Lodgings() { Id = 2, Price = 40000, Quality = "Minosegi", Hotel = "Furaszalloda", RoomNumber = "G3", Space = 2, },
                        new Lodgings() { Id = 3, Price = 60000, Quality = "Kiralyi", Hotel = "Furaszalloda", RoomNumber = "G4", Space = 2, },
                        new Lodgings() { Id = 4, Price = 10000, Quality = "Alap", Hotel = "Kecske lábszár", RoomNumber = "A1", Space = 3, },
                        new Lodgings() { Id = 5, Price = 20000, Quality = "Minosegi", Hotel = "Kecske lábszár", RoomNumber = "A2", Space = 1, },
                        new Lodgings() { Id = 6, Price = 30000, Quality = "Alap", Hotel = "Lépcsős Pincelakók", RoomNumber = "A113", Space = 6, },
            };
            mockedLodgingsRepo.Setup(repo => repo.GetAll()).Returns(lodgings.AsQueryable());
            DaraOffice daraOffice = new DaraOffice(mockedBookingRepo.Object, mockedPersoneRepo.Object, mockedBusesRepo.Object, mockedTravelRepo.Object, mockedLodgingsRepo.Object);
            var result = daraOffice.GetAllLodgins();
            mockedLodgingsRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedLodgingsRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// TestGetOne.
        /// </summary>
        [Test]
        public void TestGetOne()
        {
            Mock<IPersonsRepository> mockedPersoneRepo = new Mock<IPersonsRepository>(
                MockBehavior.Loose);
            Mock<IBusesRepository> mockedBusesRepo = new Mock<IBusesRepository>(
                MockBehavior.Loose);
            Mock<IBookingsRepository> mockedBookingRepo = new Mock<IBookingsRepository>(
                MockBehavior.Loose);
            Mock<ITravelRepository> mockedTravelRepo = new Mock<ITravelRepository>(
                MockBehavior.Loose);
            Mock<ILodgingsRepository> mockedLodgingsRepo = new Mock<ILodgingsRepository>(
                MockBehavior.Loose);

            List<Buses> buses = new List<Buses>
            {
                new Buses() { Id = 1, Capacity = 50, LicencePlate = "ABC-123", Departures = DateTime.ParseExact("2023.06.08T08:10", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2023.06.09T21:33", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 5 },
                new Buses() { Id = 2, Capacity = 20, LicencePlate = "GEG-001", Departures = DateTime.ParseExact("2022.09.08T10:11", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2022.09.09T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 7 },
                new Buses() { Id = 3, Capacity = 100, LicencePlate = "FIN-626", Departures = DateTime.ParseExact("2021.12.01T05:05", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2021.12.01T22:09", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 9 },
            };
            mockedBusesRepo.Setup(repo => repo.GetByID(2)).Returns(buses.SingleOrDefault(x => x.Id == 2));

            DaraOffice daraOffice = new DaraOffice(mockedBookingRepo.Object, mockedPersoneRepo.Object, mockedBusesRepo.Object, mockedTravelRepo.Object, mockedLodgingsRepo.Object);
            var result = daraOffice.GetBus(2);
            Buses expectedBus = new Buses() { Id = 2, Capacity = 20, LicencePlate = "GEG-001", Departures = DateTime.ParseExact("2022.09.08T10:11", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2022.09.09T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 7 };

            Assert.That(result, Is.EqualTo(expectedBus));
            mockedBusesRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// TestUpdateOne.
        /// </summary>
        [Test]
        public void TestUpateOne()
        {
            Mock<IPersonsRepository> mockedPersoneRepo = new Mock<IPersonsRepository>(
                MockBehavior.Loose);
            Mock<IBusesRepository> mockedBusesRepo = new Mock<IBusesRepository>(
                MockBehavior.Loose);
            Mock<IBookingsRepository> mockedBookingRepo = new Mock<IBookingsRepository>(
                MockBehavior.Loose);
            Mock<ITravelRepository> mockedTravelRepo = new Mock<ITravelRepository>(
                MockBehavior.Loose);
            Mock<ILodgingsRepository> mockedLodgingsRepo = new Mock<ILodgingsRepository>(
                MockBehavior.Loose);

            List<Travel> travels = new List<Travel>
            {
                new Travel() { Id = 1, Price = 20000, Starts = DateTime.ParseExact("2023.06.09", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2023.06.16", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" },
                new Travel() { Id = 2, Price = 300000, Starts = DateTime.ParseExact("2022.09.02T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2022.09.09T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" },
            };
            List<Travel> expectedTravels = new List<Travel>
            {
                new Travel() { Id = 1, Price = 20000, Starts = DateTime.ParseExact("2023.06.09", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2023.06.16", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" },
                new Travel() { Id = 2, Price = 60, Starts = DateTime.ParseExact("2022.09.02T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2022.09.09T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" },
            };
            Travel updateTravel = new Travel() { Id = 2, Price = 60, Starts = DateTime.ParseExact("2022.09.02T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2022.09.09T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" };

            mockedTravelRepo.Setup(repo => repo.Update(updateTravel))
                .Callback((Travel t) => { travels[1] = t; });
            DaraOffice daraOffice = new DaraOffice(mockedBookingRepo.Object, mockedPersoneRepo.Object, mockedBusesRepo.Object, mockedTravelRepo.Object, mockedLodgingsRepo.Object);

            daraOffice.UpdateTravels(updateTravel);

            Assert.That(expectedTravels, Is.EquivalentTo(travels));
            mockedTravelRepo.Verify(repo => repo.Update(It.IsAny<Travel>()), Times.Once);
            mockedTravelRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedTravelRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// TestDeleteOne.
        /// </summary>
        [Test]
        public void TestDeleteOne()
        {
            Mock<IPersonsRepository> mockedPersoneRepo = new Mock<IPersonsRepository>(
                MockBehavior.Loose);
            Mock<IBusesRepository> mockedBusesRepo = new Mock<IBusesRepository>(
                MockBehavior.Loose);
            Mock<IBookingsRepository> mockedBookingRepo = new Mock<IBookingsRepository>(
                MockBehavior.Loose);
            Mock<ITravelRepository> mockedTravelRepo = new Mock<ITravelRepository>(
                MockBehavior.Loose);
            Mock<ILodgingsRepository> mockedLodgingsRepo = new Mock<ILodgingsRepository>(
                MockBehavior.Loose);

            List<Bookings> bookings = new List<Bookings>
            {
                        new Bookings() { Id = 1, BusId = 1, PersonId = 2, Date = DateTime.Now, TravelId = 1, LodgingsId = 1, Seats = "42" },
                        new Bookings() { Id = 2, BusId = 2, PersonId = 2, Date = DateTime.Now, TravelId = 2, LodgingsId = 5, Seats = "22" },
                        new Bookings() { Id = 3, BusId = 3, PersonId = 1, Date = DateTime.Now, TravelId = 1, LodgingsId = 2, Seats = "39" },
                        new Bookings() { Id = 4, BusId = 3, PersonId = 3, Date = DateTime.Now, TravelId = 2, LodgingsId = 6, Seats = "11" },
            };
            Bookings deleteBook = new Bookings() { Id = 3, BusId = 3, PersonId = 1, Date = DateTime.Now, TravelId = 1, LodgingsId = 2, Seats = "39" };
            mockedBookingRepo.Setup(repo => repo.Delete(deleteBook)).Callback((Bookings b) => bookings.Remove(b));

            DaraOffice daraOffice = new DaraOffice(mockedBookingRepo.Object, mockedPersoneRepo.Object, mockedBusesRepo.Object, mockedTravelRepo.Object, mockedLodgingsRepo.Object);
            daraOffice.DeleteBooking(deleteBook);

            List<Bookings> expectedbookings = new List<Bookings>
            {
                        new Bookings() { Id = 1, BusId = 1, PersonId = 2, Date = DateTime.Now, TravelId = 1, LodgingsId = 1, Seats = "42" },
                        new Bookings() { Id = 2, BusId = 2, PersonId = 2, Date = DateTime.Now, TravelId = 2, LodgingsId = 5, Seats = "22" },
                        new Bookings() { Id = 4, BusId = 3, PersonId = 3, Date = DateTime.Now, TravelId = 2, LodgingsId = 6, Seats = "11" },
            };

            Assert.That(expectedbookings, Is.EquivalentTo(bookings));
            mockedBookingRepo.Verify(repo => repo.Delete(It.IsAny<Bookings>()), Times.Once);
            mockedBookingRepo.Verify(repo => repo.GetByID(It.IsAny<int>()), Times.Never);
            mockedBookingRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// TestNewOne.
        /// </summary>
        [Test]
        public void TestNewOne()
        {
            Mock<IPersonsRepository> mockedPersoneRepo = new Mock<IPersonsRepository>(
                MockBehavior.Loose);
            Mock<IBusesRepository> mockedBusesRepo = new Mock<IBusesRepository>(
                MockBehavior.Loose);
            Mock<IBookingsRepository> mockedBookingRepo = new Mock<IBookingsRepository>(
                MockBehavior.Loose);
            Mock<ITravelRepository> mockedTravelRepo = new Mock<ITravelRepository>(
                MockBehavior.Loose);
            Mock<ILodgingsRepository> mockedLodgingsRepo = new Mock<ILodgingsRepository>(
                MockBehavior.Loose);
            List<Persons> persons = new List<Persons>()
            {
                        new Persons() { Id = 1, Email = "Karcsi@Karcsilagzi.hu", FirstName = "Károly", LastName = "Buli", PhoneNumber = "+363033333333", Adress = "1111 Budapest XI. Balaton utca 2" },
                        new Persons() { Id = 2, Email = "Marika@mzperx.hu", FirstName = "Mariann", LastName = "Soos-Ferenczné Nagy ", PhoneNumber = "+320000351000", Adress = "8174 Balatonkenese Simon István utca 11" },
                        new Persons() { Id = 3, Email = "Zoldlevelibeka@narancsmail.hu", FirstName = "Gergely", LastName = "Csíkos", PhoneNumber = "+310011110", Adress = "8172 Balatonakarattya  Zöld-ház út 222" },
            };
            Persons newPerson = new Persons() { Id = 4, Email = "ferenczlukacs@mail.hu", FirstName = "Lukacs", LastName = "Ferecz", PhoneNumber = "+32222", Adress = "2000 Szentendre Deli Antal utca 48/3" };

            mockedPersoneRepo.Setup(repo => repo.Update(newPerson)).Callback((Persons p) => { persons.Add(p); });
            DaraOffice daraOffice = new DaraOffice(mockedBookingRepo.Object, mockedPersoneRepo.Object, mockedBusesRepo.Object, mockedTravelRepo.Object, mockedLodgingsRepo.Object);
            daraOffice.UpdatePersons(newPerson);
            List<Persons> expectedPersons = new List<Persons>()
            {
                        new Persons() { Id = 1, Email = "Karcsi@Karcsilagzi.hu", FirstName = "Károly", LastName = "Buli", PhoneNumber = "+363033333333", Adress = "1111 Budapest XI. Balaton utca 2" },
                        new Persons() { Id = 2, Email = "Marika@mzperx.hu", FirstName = "Mariann", LastName = "Soos-Ferenczné Nagy ", PhoneNumber = "+320000351000", Adress = "8174 Balatonkenese Simon István utca 11" },
                        new Persons() { Id = 3, Email = "Zoldlevelibeka@narancsmail.hu", FirstName = "Gergely", LastName = "Csíkos", PhoneNumber = "+310011110", Adress = "8172 Balatonakarattya  Zöld-ház út 222" },
                        new Persons() { Id = 4, Email = "ferenczlukacs@mail.hu", FirstName = "Lukacs", LastName = "Ferecz", PhoneNumber = "+32222", Adress = "2000 Szentendre Deli Antal utca 48/3" },
            };
            Assert.That(expectedPersons, Is.EquivalentTo(persons));
            mockedPersoneRepo.Verify(repo => repo.Update(It.IsAny<Persons>()), Times.Once);
            mockedPersoneRepo.Verify(repo => repo.GetAll(), Times.Never);
        }
    }
}
