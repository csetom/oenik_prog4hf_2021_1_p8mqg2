﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("Usage", "CA2213:Disposable fields should be disposed", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1202:Elements should be ordered by access", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<NikGitStats>", Scope = "member", Target = "~P:DaraZrt.WpfClient.MainVM.AllPersons")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<NikGitStats>", Scope = "member", Target = "~M:DaraZrt.WpfClient.MainLogic.ApiGetPersons~System.Collections.Generic.List{DaraZrt.WpfClient.PersonsVM}")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<NikGitStats>", Scope = "member", Target = "~M:DaraZrt.WpfClient.MainLogic.ApiEditPerson(DaraZrt.WpfClient.PersonsVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<NikGitStats>", Scope = "member", Target = "~M:DaraZrt.WpfClient.MainLogic.ApiEditPerson(DaraZrt.WpfClient.PersonsVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<NikGitStats>", Scope = "member", Target = "~M:DaraZrt.WpfClient.MainLogic.ApiEditPerson(DaraZrt.WpfClient.PersonsVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<NikGitStats>", Scope = "member", Target = "~M:DaraZrt.WpfClient.MainLogic.ApiDelPersons(DaraZrt.WpfClient.PersonsVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<NikGitStats>", Scope = "member", Target = "~M:DaraZrt.WpfClient.MainLogic.ApiGetPersons~System.Collections.Generic.List{DaraZrt.WpfClient.PersonsVM}")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<NikGitStats>", Scope = "member", Target = "~M:DaraZrt.WpfClient.MainLogic.ApiDelPersons(DaraZrt.WpfClient.PersonsVM)")]
