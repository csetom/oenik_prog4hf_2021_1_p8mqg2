﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfClient
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Register<string>(this, "PersonsResult", msg =>
            {
                (DataContext as MainVM).LoadCmd.Execute(null);
                MessageBox.Show(msg);
            });
            (DataContext as MainVM).EditorFunc = (persons) =>
            {
                EditorWindow win = new EditorWindow(persons);
                return win.ShowDialog() == true;
            };
        }

        private void Window_Closing(
            object sender,
            System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
