﻿// <copyright file="PersonsVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfClient
{
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Person Vm.
    /// </summary>
    public class PersonsVM : ObservableObject
    {
        private int id;
        private string firstName;
        private string lastName;
        private string adress;
        private string phoneNumber;
        private string email;

        /// <summary>
        /// Gets or sets id get,set.
        /// </summary>
        public int Id { get => id; set => Set(ref id, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string FirstName { get => firstName; set => Set(ref firstName, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string LastName { get => lastName; set => Set(ref lastName, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string Adress { get => adress; set => Set(ref adress, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string PhoneNumber { get => phoneNumber; set => Set(ref phoneNumber, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string Email { get => email; set => Set(ref email, value); }

        /// <summary>
        /// Copy Person.
        /// </summary>
        /// <param name="other">The other Person.</param>
        public void CopyFrom(PersonsVM other)
        {
            GetType().GetProperties().ToList().
                ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
