﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;

[assembly: System.CLSCompliant(false)]

namespace DaraZrt.WpfClient
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Main View MOdel.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private PersonsVM selectedPersons;
        private ObservableCollection<PersonsVM> allPersons;

        /// <summary>
        /// Gets or sets AllPersons.
        /// </summary>
        public ObservableCollection<PersonsVM> AllPersons
        {
            get { return allPersons; }
            set { Set(ref allPersons, value); }
        }

        /// <summary>
        /// Gets or sets selected Persons.
        /// </summary>
        public PersonsVM SelectedPersons
        {
            get { return selectedPersons; }
            set { Set(ref selectedPersons, value); }
        }

        /// <summary>
        /// Gets or sets EditorFunc.
        /// </summary>
        public Func<PersonsVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets AddcCmd.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets DelCmd.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets ModCmd.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets LoadCmd.
        /// </summary>
        public ICommand LoadCmd { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="klogic">Logic.</param>
        public MainVM(MainLogic klogic)
        {
            this.logic = klogic;

            if (IsInDesignMode)
            {
                return;
            }

            LoadCmd = new RelayCommand(() => AllPersons = new ObservableCollection<PersonsVM>(logic.ApiGetPersons()));
            DelCmd = new RelayCommand(() => logic.ApiDelPersons(selectedPersons));
            AddCmd = new RelayCommand(() => logic.EditPersons(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditPersons(selectedPersons, EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null :
                  ServiceLocator.Current.GetInstance<MainLogic>())
        {
        }
    }
}
