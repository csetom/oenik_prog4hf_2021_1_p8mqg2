﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// MainLogic.
    /// </summary>
    public class MainLogic : IDisposable
    {
        private string url = "http://localhost:5000/PersonsApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new
            JsonSerializerOptions(JsonSerializerDefaults.Web);

        private static void SendMessage(bool succses)
        {
            string msg = succses ? "Operation susscesfull" : "Operation failed";
            Messenger.Default.Send(msg, "PersonsResult");
        }

        /// <summary>
        /// Get All Persons.
        /// </summary>
        /// <returns>List of persons.</returns>
        public List<PersonsVM> ApiGetPersons()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonSerializer.Deserialize<List<PersonsVM>>(json, jsonOptions);
            return list;
        }

        /// <summary>
        /// ApiDelPersons.
        /// </summary>
        /// <param name="persons">Persons.</param>
        public void ApiDelPersons(PersonsVM persons)
        {
            bool succses = false;
            if (persons != null)
            {
                string json = client.GetStringAsync(url + "del/" + persons.Id.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                succses = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(succses);
        }

        /// <summary>
        /// ApiEditPErsons.
        /// </summary>
        /// <param name="persons">Persons.</param>
        /// <param name="isEditing">IsEditing.</param>
        /// <returns>True False.</returns>
        public bool ApiEditPerson(PersonsVM persons, bool isEditing)
        {
            if (persons == null)
            {
                return false;
            }

            string myUrl = url + (isEditing ? "mod" : "add");
            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("id", persons.Id.ToString());
            }

            postData.Add("firstName", persons.FirstName.ToString());
            postData.Add("lastName", persons.LastName.ToString());
            postData.Add("adress", persons.Adress.ToString());
            postData.Add("email", persons.Email.ToString());
            postData.Add("phoneNumber", persons.PhoneNumber.ToString());
            string json = client.PostAsync(myUrl, content: new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Edit Persons.
        /// </summary>
        /// <param name="persons">Persons.</param>
        /// <param name="editorFunc">EditorFunc.</param>
        public void EditPersons(PersonsVM persons, Func<PersonsVM, bool> editorFunc)
        {
            PersonsVM clone = new PersonsVM();
            if (persons != null)
            {
                clone.CopyFrom(persons);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (persons != null)
                {
                    success = ApiEditPerson(clone, true);
                }
                else
                {
                    success = ApiEditPerson(clone, false);
                }
            }

            SendMessage(success == true);
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="MainLogic"/> class.
        /// Destruct.
        /// </summary>
        ~MainLogic()
        {
            Dispose(false);
        }

        /// <summary>
        /// Dispose.
        /// </summary>
        /// <param name="disposing">true or false.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                client.Dispose();
            }
        }
    }
}
