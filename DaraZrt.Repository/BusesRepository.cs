﻿// <copyright file="BusesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DaraZrt.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Bus Repository.
    /// </summary>
    public class BusesRepository : IBusesRepository
    {
        /// <summary>
        /// Database context.
        /// </summary>
        private readonly DbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusesRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database Context.</param>
        public BusesRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <inheritdoc/>
        public int Add(Buses entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SaveChanges.
        /// </summary>
        public void Commit()
        {
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="entity"> Entity what I want to delete.</param>
        public void Delete(Buses entity)
        {
            this.dbContext.Remove(entity);
        }

        /// <summary>
        /// Find.
        /// </summary>
        /// <param name="filter">The filter, to find one or more entity.</param>
        /// <returns>The entities.</returns>
        public IEnumerable<Buses> Find(Func<Buses, bool> filter)
        {
            return this.GetAll().Where(filter);
        }

        /// <summary>
        /// Everyone from the dbContext.
        /// </summary>
        /// <returns> Every entity.</returns>
        public IQueryable<Buses> GetAll()
        {
            return this.dbContext.Set<Buses>();
        }

        /// <summary>
        /// One entity by id.
        /// </summary>
        /// <param name="id"> The Id.</param>
        /// <returns>The entity.</returns>
        public Buses GetByID(int id)
        {
            return this.GetAll().SingleOrDefault(inditottBuszok => inditottBuszok.Id == id);
        }

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Busses Entity.</param>
        public void Update(Buses entity)
        {
            this.dbContext.Update(entity);
        }
    }
}
