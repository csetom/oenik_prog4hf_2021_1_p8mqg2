﻿// <copyright file="TravelRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DaraZrt.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Travel Repository.
    /// </summary>
    public class TravelRepository : ITravelRepository
    {
        /// <summary>
        /// Database context.
        /// </summary>
        private readonly DbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="TravelRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database Context.</param>
        public TravelRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <inheritdoc/>
        public int Add(Travel entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SaveChanges.
        /// </summary>
        public void Commit()
        {
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="entity"> Entity what I want to delete.</param>
        public void Delete(Travel entity)
        {
            this.dbContext.Remove(entity);
        }

        /// <summary>
        /// Find.
        /// </summary>
        /// <param name="filter">The filter, to find one or more entity.</param>
        /// <returns>The entities.</returns>
        public IEnumerable<Travel> Find(Func<Travel, bool> filter)
        {
            return this.GetAll().Where(filter);
        }

        /// <summary>
        /// Everyone from the dbContext.
        /// </summary>
        /// <returns> Every entity.</returns>
        public System.Linq.IQueryable<Travel> GetAll()
        {
            return (IQueryable<Travel>)this.dbContext.Set<Travel>();
        }

        /// <summary>
        /// One entity by id.
        /// </summary>
        /// <param name="id"> The Id.</param>
        /// <returns>The entity.</returns>
        public Travel GetByID(int id)
        {
            return this.GetAll().SingleOrDefault(utazas => utazas.Id == id);
        }

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Travel Entity.</param>
        public void Update(Travel entity)
        {
            this.dbContext.Update(entity);
        }
    }
}
