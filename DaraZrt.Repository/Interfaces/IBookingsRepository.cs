﻿// <copyright file="IBookingsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DaraZrt.Data.Models;

    /// <summary>
    /// Bookings repository.
    /// </summary>
    public interface IBookingsRepository : IRepository<Bookings>
    {
    }
}
