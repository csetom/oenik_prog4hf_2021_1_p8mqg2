﻿// <copyright file="ITravelRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DaraZrt.Data.Models;

    /// <summary>
    /// Travel repository interface.
    /// </summary>
    public interface ITravelRepository : IRepository<Travel>
    {
    }
}
