﻿// <copyright file="ILodgingsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DaraZrt.Data.Models;

    /// <summary>
    /// SzallasRepository interface.
    /// </summary>
    public interface ILodgingsRepository : IRepository<Lodgings>
    {
    }
}
