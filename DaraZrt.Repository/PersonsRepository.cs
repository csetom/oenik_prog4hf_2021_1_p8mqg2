﻿// <copyright file="PersonsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DaraZrt.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Utas repository.
    /// </summary>
    public class PersonsRepository : IPersonsRepository
    {
        /// <summary>
        /// Database context.
        /// </summary>
        private readonly DbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonsRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database Context.</param>
        public PersonsRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// SaveChanges.
        /// </summary>
        public void Commit()
        {
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="entity"> Entity what I want to delete.</param>
        public void Delete(Persons entity)
        {
            this.dbContext.Remove(entity);
        }

        /// <summary>
        /// Find.
        /// </summary>
        /// <param name="filter">The filter, to find one or more entity.</param>
        /// <returns>The entities.</returns>
        public IEnumerable<Persons> Find(Func<Persons, bool> filter)
        {
            return this.GetAll().Where(filter);
        }

        /// <summary>
        /// Everyone from the dbContext.
        /// </summary>
        /// <returns> Every entity.</returns>
        public System.Linq.IQueryable<Persons> GetAll()
        {
            return (IQueryable<Persons>)this.dbContext.Set<Persons>().AsNoTracking();
        }

        /// <summary>
        /// One entity by id.
        /// </summary>
        /// <param name="id"> The Id.</param>
        /// <returns>The entity.</returns>
        public Persons GetByID(int id)
        {
            return this.GetAll().SingleOrDefault(utas => utas.Id == id);
        }

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Utas Entity.</param>
        public void Update(Persons entity)
        {
            this.dbContext.Update(entity);
            this.dbContext.SaveChanges();
            this.dbContext.Entry(entity).State = EntityState.Detached;
        }

        /// <summary>
        /// Add entity.
        /// </summary>
        /// <param name="entity">Utas Entity.</param>
        /// <returns>Id.</returns>
        public int Add(Persons entity)
        {
            int id = 0;
            if (entity != null)
            {
                this.dbContext.Add(entity);
                this.dbContext.SaveChanges();
                id = entity.Id;
                this.dbContext.Entry(entity).State = EntityState.Detached;
            }

            return id;
        }
    }
}
