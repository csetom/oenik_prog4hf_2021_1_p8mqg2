﻿// <copyright file="BookingsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DaraZrt.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Bookings repository.
    /// </summary>
    public class BookingsRepository : IBookingsRepository
    {
        /// <summary>
        /// Database context.
        /// </summary>
        private readonly DbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookingsRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database Context.</param>
        public BookingsRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <inheritdoc/>
        public int Add(Bookings entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SaveChanges.
        /// </summary>
        public void Commit()
        {
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="entity"> Entity what I want to delete.</param>
        public void Delete(Bookings entity)
        {
            this.dbContext.Remove(entity);
        }

        /// <summary>
        /// Find.
        /// </summary>
        /// <param name="filter">The filter, to find one or more entity.</param>
        /// <returns>The entities.</returns>
        public IEnumerable<Bookings> Find(Func<Bookings, bool> filter)
        {
            return this.GetAll().Where(filter);
        }

        /// <summary>
        /// Everyone from the dbContext.
        /// </summary>
        /// <returns> Every entity.</returns>
        public IQueryable<Bookings> GetAll()
        {
            return this.dbContext.Set<Bookings>();
        }

        /// <summary>
        /// One entity by id.
        /// </summary>
        /// <param name="id"> The Id.</param>
        /// <returns>The entity.</returns>
        public Bookings GetByID(int id)
        {
            return this.GetAll().SingleOrDefault(utfoglalas => utfoglalas.Id == id);
        }

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Bookings Entity.</param>
        public void Update(Bookings entity)
        {
            this.dbContext.Update(entity);
        }
    }
}
