﻿// <copyright file="LodgingsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DaraZrt.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Szallas Repository.
    /// </summary>
    public class LodgingsRepository : ILodgingsRepository
    {
        /// <summary>
        /// Database context.
        /// </summary>
        private readonly DbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="LodgingsRepository"/> class.
        /// </summary>
        /// <param name="dbContext">Database Context.</param>
        public LodgingsRepository(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Add.
        /// </summary>
        /// <param name="entity">Lodgings.</param>
        /// <returns>ID.</returns>
        public int Add(Lodgings entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SaveChanges.
        /// </summary>
        public void Commit()
        {
            this.dbContext.SaveChanges();
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="entity"> Entity what I want to delete.</param>
        public void Delete(Lodgings entity)
        {
            this.dbContext.Remove(entity);
        }

        /// <summary>
        /// Find.
        /// </summary>
        /// <param name="filter">The filter, to find one or more entity.</param>
        /// <returns>The entities.</returns>
        public IEnumerable<Lodgings> Find(Func<Lodgings, bool> filter)
        {
            return this.GetAll().Where(filter);
        }

        /// <summary>
        /// Everyone from the dbContext.
        /// </summary>
        /// <returns> Every entity.</returns>
        public System.Linq.IQueryable<Lodgings> GetAll()
        {
            return (IQueryable<Lodgings>)this.dbContext.Set<Lodgings>();
        }

        /// <summary>
        /// One entity by id.
        /// </summary>
        /// <param name="id"> The Id.</param>
        /// <returns>The entity.</returns>
        public Lodgings GetByID(int id)
        {
            return this.GetAll().SingleOrDefault(szallas => szallas.Id == id);
        }

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Szallas Entity.</param>
        public void Update(Lodgings entity)
        {
            this.dbContext.Update(entity);
        }
    }
}
