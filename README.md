# Miről fog szólni: #

A Dubai Alkalmi Rendezvényekre Autóbuszos-uztatás Zárt Részvény Tárasaság az az a DARA ZRT. üdülési cégnek buszos utazásait nyilvántartó rendszer. 

* Persons:(Id, FirstName, LastName, Adress, Phone, E-mail)
* Travel:(Id, Starts, Ends, Country, City, Price)
* Buses:(Id, LicencePlate, StopNumber, Capacity, Departure, Arrival)
* Lodgings (Id, Hotel, RoomNumber, Price, Space, Quality)
* Bookings (Id, PersonId, TravelId, BusId, HotelId, Date, Seats)

### Funkció Lista ###

- Persons List / List One / Add / Modify / Delete
- Travels List / List One / Add / Modify / Delete
- Buses List / List One / Add / Modify / Delete
- Lodgings List / List One / Add / Modify / Delete
- Bookings List / List One / Add / Modify / Delete
- Cheap Bookings that are summa Price (Lodging+Travel) is less than 50000 Ft.
- Buses that has more bookable seat but already is booked. 
- People, who booked a summer trip.




