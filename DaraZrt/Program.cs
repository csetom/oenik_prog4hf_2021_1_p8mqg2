﻿using DaraZrt.Data;
using System;
using System.Collections.Generic;
using DaraZrt.Logic;
using DaraZrt.Repository;
using System.Linq;
using ConsoleTools;
using DaraZrt.Data.Models;
using System.Threading.Tasks;
using DaraZrt.Logic.Models;

namespace DaraZrt
{
    /// <summary>
    ///  Extension. From 
    /// </summary>
    static class Extensions
    {
        public static void ToConsole<T>(this IEnumerable<T> input, string str)
        {
            Console.WriteLine("*** BEGIN " + str+ "***");
            foreach (T item in input)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine("*** END  "+ str+ " ***");
            Console.WriteLine("*** Press Any Key ***");
            Console.ReadLine();
        }
    }
    class Program
    {
     
            static void Main(string[] args)
        {

           

            using (var ctx = new DaraDBContext())
            {
                DaraLogic daraLogic = new DaraLogic(
                    new BookingsRepository(ctx),
                    new PersonsRepository(ctx),
                    new BusesRepository(ctx),
                    new TravelRepository(ctx),
                    new LodgingsRepository(ctx)

                    );

                DaraOffice daraOffice = daraLogic.DaraOffice;
                DaraStatistics daraStatistics = daraLogic.DaraStatistics;

            var subExtraMenu = new ConsoleMenu(args, level: 1)
                    .Add("Bookable Buses, Where there is free space", ()=> { daraStatistics.GetBookableBusses().Select(x => $"{x.Id} {x.LicencePlate} : {x.NumberOfFreeSeats}/{x.NumberOfSeats}").ToConsole("Free Seats"); })
                    .Add("People Who Brought Travel for Summer", () => { daraStatistics.GetSummerPeople().Select(x => $"{x.Id} {x.FirstName} {x.LastName} : {x.City} {x.Start} - {x.End}").ToConsole("Summer People"); })
                    .Add("Cheap bookings (Summa Price<50000)", (Action)(() => { Enumerable.Select<CheapBooking, string>(daraStatistics.GetCheapBooking(), (Func<CheapBooking, string>)(x => (string)$"{x.Id}; {x.Price}; {x.City};  {x.Hotel}, {x.Quality} ;  {x.Starts} - {x.Ends}")).ToConsole("Cheap Booking"); }))
                    .Add("<Back", ConsoleMenu.Close)
                    .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.EnableFilter = true;
                    config.Title = "Lists ";
                    config.EnableBreadcrumb = true;
                    config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                });
                var subExtraAsyncMenu = new ConsoleMenu(args, level: 1)
                .Add("Bookable Buses, Where there is free space", () => { Task<ICollection<BookableBusses>> task = daraStatistics.GetBookableBussesAsync(); Console.WriteLine("Im Working");  task.Wait(); task.Result.Select(x => $"{x.Id} {x.LicencePlate} : {x.NumberOfFreeSeats}/{x.NumberOfSeats}").ToConsole("Free Seats"); })
                 .Add("People Who Brought Travel for Summer", () => { Task<ICollection<SummerPeople>> task = daraStatistics.GetSummerPeopleAsync(); Console.WriteLine("Im Working"); task.Wait(); task.Result.Select(x => $"{x.Id} {x.FirstName} {x.LastName} : {x.City} {x.Start} - {x.End}").ToConsole("Summer People"); })
                .Add("Cheap bookings (Summa Price<50000)", (Action)(() => { Task<ICollection<CheapBooking>> task = daraStatistics.GetCheapBookingAsync(); Console.WriteLine("Im Working"); task.Wait(); task.Result.Select(x => $"{x.Id}; {x.Price}; {x.City};  {x.Hotel}, {x.Quality} ;  {x.Starts} - {x.Ends}").ToConsole("Cheap Booking"); }))
                .Add("<Back", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.ClearConsole = true;
                    config.Selector = "--> ";
                    config.EnableFilter = true;
                    config.Title = "Lists ";
                    config.EnableBreadcrumb = true;
                    config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                });
                var subListMenu = new ConsoleMenu(args, level: 1)
                .Add("List all Person", () => daraOffice.GetAllPerson().Select(x=> $"Id: {x.Id}, FirstName: {x.FirstName}, LastName: {x.LastName}, Address: {x.Adress}, PhoneNumber:{x.PhoneNumber}, Email: {x.Email}").ToConsole("Person"))
                .Add("List all Busses", () => daraOffice.GetAllBuses().Select(x => $"{x.Id},{x.LicencePlate},{x.Departures},{x.Arrival},{x.Capacity},{x.StopNumber}").ToConsole("Busses"))
                .Add("List all Lodgings", () => daraOffice.GetAllLodgins().Select(x => $"{x.Id},{x.Hotel},{x.RoomNumber},{x.Quality},{x.Space},{x.Price}").ToConsole("Lodgings"))
                .Add("List all Travel", () => daraOffice.GetAllTravel().Select(x => $"{x.Id},{x.Country},{x.City},{x.Starts},{x.Ends},{x.Price}").ToConsole("Travel"))
                .Add("List all Booking", () => daraOffice.GetAllBookings().Select(x => $"{x.Id}: {x.Persons.FirstName},{x.Persons.LastName} \n  {x.Buses.LicencePlate} : {x.Seats}, \n  {x.Travel.Country} {x.Travel.City}, \n  {x.Lodgings.Hotel}: {x.Lodgings.RoomNumber}, \n  {x.Travel.Price}").ToConsole("Booking"))
                .Add("<Back", ConsoleMenu.Close)
                .Configure(config =>
                {
                    
                    config.Selector = "--> ";
                    config.EnableFilter = true;
                    config.Title = "Lists ";
                    config.EnableBreadcrumb = true;
                    config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                });
                var subDeleteMenu = new ConsoleMenu(args, level: 2)
                          .Add("Delete Person", () =>
                          {
                              Console.WriteLine("Wich Person do you want to Delete?");
                              daraOffice.GetAllPerson().Select(x => $"Id: {x.Id}, FirstName: {x.FirstName}").ToConsole("Person");
                              Console.Write("Person Id: ");
                              string input = Console.ReadLine();

                              try
                              {
                                  int id = Int32.Parse(input);
                                  daraOffice.DeletePersons(daraOffice.GetPerson(id));
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();

                          })
                          .Add("Delete Bus", () =>
                          {
                              Console.WriteLine("Wich Bus do you want to Delete?");
                              daraOffice.GetAllBuses().Select(x => $"{x.Id},{x.LicencePlate},{x.Departures}").ToConsole("Busses");
                              Console.Write("Bus Id: ");
                              string input = Console.ReadLine();

                              try
                              {
                                  int id = Int32.Parse(input);
                                  daraOffice.DeleteBuses(daraOffice.GetBus(id));
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();

                          })
                          .Add("Delete Lodgings", () =>
                          {
                              Console.WriteLine("Wich Lodgings do you want to Delete?");
                              daraOffice.GetAllLodgins().Select(x => $"{x.Id},{x.Hotel},{x.RoomNumber}").ToConsole("Hotels");
                              Console.Write("Hotel Id: ");
                              string input = Console.ReadLine();
                              try
                              {
                                  int id = Int32.Parse(input);
                                  daraOffice.DeleteLodgings(daraOffice.GetLodging(id));
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();

                          })
                          .Add("Delete Travel", () =>
                          {
                              Console.WriteLine("Wich Travel do you want to Delete?");
                              daraOffice.GetAllTravel().Select(x => $"{x.Id},{x.City},{x.Starts}").ToConsole("Travels");
                              Console.Write("Travel ID: ");
                              string input = Console.ReadLine();

                              try
                              {
                                  int id = Int32.Parse(input);
                                  daraOffice.DeleteTravel(daraOffice.GetTravel(id));
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();

                          })
                          .Add("Delete Booking", () =>
                          {
                          Console.WriteLine("Wich Booking do you want to Delete?");
                          daraOffice.GetAllBookings().Select(x => $"{x.Id}: {x.Persons.FirstName},{x.Persons.LastName} \n  {x.Buses.LicencePlate} : {x.Seats}, \n {x.Travel.City}, {x.Travel.Starts}\n  {x.Lodgings.Hotel}: {x.Lodgings.RoomNumber}").ToConsole("Booking");
                              Console.Write("Booking Id:");
                              string input = Console.ReadLine();

                              try
                              {
                                  int id = Int32.Parse(input);
                                  daraOffice.DeleteBooking(daraOffice.GetBooking(id));
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();
                          })

                           .Add("<Back", ConsoleMenu.Close)
                           .Configure(config =>
                           {
                               config.Selector = "--> ";
                               config.EnableFilter = false;
                               config.Title = "Delete ";
                               config.EnableBreadcrumb = true;
                               config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                           }); ;
                
           var subUpdateMenu = new ConsoleMenu(args,1)
                          .Add("Update Person", () =>
                          {
                              Console.WriteLine("Wich Person do you want to Update?");
                              daraOffice.GetAllPerson().Select(x => $"Id: {x.Id}, FirstName: {x.FirstName}").ToConsole("Person");
                              Console.Write("Person Id: ");
                              string input = Console.ReadLine();

                              try
                              {
                                  int id = Int32.Parse(input);
                                  Persons person = daraOffice.GetPerson(id);
                                  Console.WriteLine("Wich Attribute do you want to Update?");
                                

                                  var subPersonMenu = new ConsoleMenu(args, 2)
                                  .Add("FirstName: " + person.FirstName,()=> { Console.Write("New Attribute: "); person.FirstName = Console.ReadLine();})
                                  .Add("LastName: " + person.LastName, () => { Console.Write("New Attribute: "); person.LastName = Console.ReadLine(); })
                                  .Add("Adress: " + person.Adress, () => { Console.Write("New Attribute: "); person.Adress = Console.ReadLine(); })
                                  .Add("PhoneNumber: " + person.PhoneNumber, () => { Console.Write("New Attribute: "); person.PhoneNumber = Console.ReadLine(); })
                                  .Add("Email: " + person.Email, () => { Console.Write("New Attribute: "); person.Email = Console.ReadLine(); })
                                  .Add("Save",  ConsoleMenu.Close)
                                   .Configure(config =>
                                   {
                                       config.ClearConsole = true;
                                       config.Selector = "--> ";
                                       config.EnableFilter = false;
                                       config.Title = "Attribues ";
                                       config.EnableBreadcrumb = true;
                                       config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                   });
                                  subPersonMenu.Show();
                                  daraOffice.UpdatePersons(person);
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();

                          })
                          .Add("Update Bus", () =>
                          {
                              Console.WriteLine("Wich Bus do you want to Update?");
                              daraOffice.GetAllBuses().Select(x => $"{x.Id},{x.LicencePlate},{x.Departures}").ToConsole("Busses");
                              Console.Write("Bus Id: ");

                              string input = Console.ReadLine();

                              try
                              {
                                  int id = Int32.Parse(input);
                                  Buses bus = daraOffice.GetBus(id);
                                  Console.WriteLine("Wich Attribute do you want to Update?");
                                  // .Add("List all Busses", () => daraOffice.GetAllBuses().Select(x => $"{x.Id},{x.LicencePlate},{x.Departures},{x.Arrival},{x.Capacity},{x.StopNumber}").ToConsole("AllBusses"))
                                  //DateTime.ParseExact("2023.06.08T08:10", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture),
                                  var subPersonMenu = new ConsoleMenu(args, 2)
                                  .Add("LicencePlate: " + bus.LicencePlate, () => { Console.Write("New Attribute: "); bus.LicencePlate = Console.ReadLine(); })
                                  .Add("Departures: " + bus.Departures.ToString(), () => {
                                      Console.Write("New Attribute: ");
                                      try { Console.WriteLine("yyy.MM.ddTHH:mm"); bus.Departures = DateTime.ParseExact(Console.ReadLine(), "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture); }
                                      catch (FormatException)
                                      {
                                          Console.WriteLine("Unable to parse");
                                      }
                                  })
                                  .Add("Arrival: " + bus.Arrival.ToString(), () => {
                                      Console.Write("New Attribute: ");
                                      try { Console.WriteLine("yyy.MM.ddTHH:mm"); bus.Arrival = DateTime.ParseExact(Console.ReadLine(), "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture); }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("Capacity: " + bus.Capacity, () =>
                                  {
                                      Console.Write("New Attribute: ");
                                      try { bus.Capacity = Int32.Parse(Console.ReadLine()); }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("StopNumber: " + bus.StopNumber, () => {
                                      Console.Write("New Attribute: ");
                                      try { bus.StopNumber = Int32.Parse(Console.ReadLine()); }
                                      catch (FormatException)
                                      {
                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("Save", ConsoleMenu.Close)
                                   .Configure(config =>
                                   {
                                       config.ClearConsole = true;
                                       config.Selector = "--> ";
                                       config.EnableFilter = false;
                                       config.Title = "Attributes";
                                       config.EnableBreadcrumb = false;
                                       config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                   });
                                  subPersonMenu.Show();
                                  daraOffice.UpdateBuses(bus);
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();

                          })
                          .Add("Update Lodgings", () =>
                          {
                              Console.WriteLine("Wich Lodging do you want to Update?");
                              daraOffice.GetAllLodgins().Select(x => $"{x.Id},{x.Hotel},{x.RoomNumber}").ToConsole("Hotels");
                              Console.Write("Hotel Id: ");
                              string input = Console.ReadLine();

                              try
                              {
                                  int id = Int32.Parse(input);
                                  Lodgings lodging = daraOffice.GetLodging(id);
                                  Console.WriteLine("Wich Attribute do you want to Update?");
                                  var subLodgingsMenu = new ConsoleMenu(args, 2)
                                  .Add("RoomNumber: " + lodging.RoomNumber, () => { Console.Write("New Attribute: "); lodging.RoomNumber = Console.ReadLine(); })
                                  .Add("Hotel: " + lodging.Hotel, () => { Console.Write("New Attribute: "); lodging.Hotel = Console.ReadLine(); })
                                  .Add("Quality: " + lodging.Quality, () => { Console.Write("New Attribute: "); lodging.Quality = Console.ReadLine(); })
                                  .Add("Space: " + lodging.Space, () => {
                                      Console.Write("New Attribute: ");
                                      try { lodging.Space = Int32.Parse(Console.ReadLine()); }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("Price: " + lodging.Price, () => {
                                      Console.Write("New Attribute: ");
                                      try { lodging.Price = Int32.Parse(Console.ReadLine()); }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("Save", ConsoleMenu.Close)
                                   .Configure(config =>
                                   {
                                       config.ClearConsole = true;
                                       config.Selector = "--> ";
                                       config.EnableFilter = false;
                                       config.Title = "Attributes";
                                       config.EnableBreadcrumb = false;
                                       config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                   });
                                  subLodgingsMenu.Show();
                                  daraOffice.UpdateLodgings(lodging);
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();
                          })
                          .Add("Update Travel", () =>
                          {
                              Console.WriteLine("Wich Lodging do you want to Update?");
                              daraOffice.GetAllTravel().Select(x => $"{x.Id},{x.City},{x.Starts}").ToConsole("Travels");
                              Console.Write("Travel ID: ");
                              string input = Console.ReadLine();
                              //$"{x.Id},{x.Country},{x.City},{x.Starts},{x.Ends},{x.Price}").ToConsole("Travel"))

                              try
                              {
                                  int id = Int32.Parse(input);
                                  Travel travel= daraOffice.GetTravel(id);
                                  Console.WriteLine("Wich Attribute do you want to Update?");
                                  var subLodgingsMenu = new ConsoleMenu(args, 2)
                                  .Add("Country: " + travel.Country, () => { Console.Write("New Attribute: "); travel.Country = Console.ReadLine(); })
                                  .Add("City: " + travel.City, () => { Console.Write("New Attribute: "); travel.City = Console.ReadLine(); })
                                  .Add("Starts: " + travel.Starts, () => {
                                      Console.Write("New Attribute: ");

                                      try { Console.WriteLine("yyyy.MM.ddTHH:mm"); travel.Starts = DateTime.ParseExact(Console.ReadLine(), "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture); }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })                             
                                  .Add("Ends: " + travel.Ends, () => {
                                      Console.Write("New Attribute: ");

                                      try { Console.WriteLine("yyyy.MM.ddTHH:mm"); travel.Ends = DateTime.ParseExact(Console.ReadLine(), "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture); }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("Price: " + travel.Price, () => {
                                      Console.Write("New Attribute: ");
                                      try { travel.Price = Int32.Parse(Console.ReadLine()); }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("Save", ConsoleMenu.Close)
                                   .Configure(config =>
                                   {
                                       config.ClearConsole = true;
                                       config.Selector = "--> ";
                                       config.EnableFilter = false;
                                       config.Title = "Attributes";
                                       config.EnableBreadcrumb = false;
                                       config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                   });
                                  subLodgingsMenu.Show();
                                  daraOffice.UpdateTravels(travel);
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();

                          })
                          .Add("Update Booking", () =>
                          {
                              Console.WriteLine("Wich Booking do you want to Update?");
                              daraOffice.GetAllBookings().Select(x => $"{x.Id}: {x.Persons.FirstName},{x.Persons.LastName} \n  {x.Buses.LicencePlate} : {x.Seats}, \n {x.Travel.City}, {x.Travel.Starts}\n  {x.Lodgings.Hotel}: {x.Lodgings.RoomNumber}").ToConsole("Booking");
                              Console.Write("Booking Id:");
                              string input = Console.ReadLine();
                              //$"{x.Id},{x.Country},{x.City},{x.Starts},{x.Ends},{x.Price}").ToConsole("Travel"))

                              try
                              {
                                  int id = Int32.Parse(input);
                                  Bookings booking = daraOffice.GetBooking(id);
                                  Console.WriteLine("Wich Attribute do you want to Update?");
                                  var subLodgingsMenu = new ConsoleMenu(args, 2)
                                  .Add("Person: "+ booking.PersonId +", "+ booking.Persons.FirstName +" "+ booking.Persons.LastName, () => { daraOffice.GetAllPerson().Select(x => $"{x.Id} {x.FirstName} {x.LastName}").ToConsole("Persons");
                                      try
                                      {
                                          Console.Write("Person ID: ");
                                          booking.PersonId = Int32.Parse(Console.ReadLine()); }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("Travel: " + booking.TravelId + ", " + booking.Travel.City+ " " + booking.Travel.Starts.ToString(), () => {
                                      daraOffice.GetAllTravel().Select(x => $"{x.Id} {x.City} {x.Starts}").ToConsole("Travels");
                                      try
                                      {
                                          Console.Write("Travel ID: ");
                                          booking.TravelId = Int32.Parse(Console.ReadLine());
                                      }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("Buses: " + booking.BusId + ", " + booking.Buses.LicencePlate + " " + booking.Buses.Departures.ToString(), () => {
                                      daraOffice.GetAllBuses().Select(x => $"{x.Id} {x.LicencePlate} {x.Departures}").ToConsole("Bueses");
                                      try
                                      {
                                          Console.Write("Bus ID: ");
                                          booking.BusId = Int32.Parse(Console.ReadLine());
                                      }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                  .Add("Lodgings: " + booking.LodgingsId + ", " + booking.Lodgings.Hotel + " " + booking.Lodgings.RoomNumber, () => {
                                      daraOffice.GetAllLodgins().Select(x => $"{x.Id} {x.Hotel} {x.RoomNumber}").ToConsole("Lodgings");
                                      try
                                      {
                                          Console.Write("Lodging ID: ");
                                          booking.LodgingsId = Int32.Parse(Console.ReadLine());
                                      }
                                      catch (FormatException)
                                      {

                                          Console.WriteLine("Unable to parse");
                                          PressAnyKey();
                                      }
                                  })
                                 .Add("Seats on Bus: " + booking.Seats , () => {
                                     Console.Write("Seats: ");
                                     booking.Seats = Console.ReadLine(); 
                                 })
                                  .Add("Save", ConsoleMenu.Close)
                                   .Configure(config =>
                                   {
                                       config.ClearConsole = true;
                                       config.Selector = "--> ";
                                       config.EnableFilter = false;
                                       config.Title = "Attributes";
                                       config.EnableBreadcrumb = false;
                                       config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                   });
                                  subLodgingsMenu.Show();
                                  daraOffice.UpdateBooking(booking);
                              }
                              catch (FormatException)
                              {
                                  Console.WriteLine($"Unable to parse '{input}'");
                              }
                              PressAnyKey();

                          })
                    .Add("<Back", ConsoleMenu.Close)
                    .Configure(config =>
           {
               config.Selector = "--> ";
               config.EnableFilter = true;
               config.Title = "Delete ";
               config.EnableBreadcrumb = true;
               config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
           });
                var subNewMenu = new ConsoleMenu(args, 1)
                               .Add("New Person", () =>
                               {
                                   Persons person = new Persons();
                                       var subPersonMenu = new ConsoleMenu(args, 2)
                                       .Add("FirstName: " + person.FirstName, () => { Console.Write("New Attribute: "); person.FirstName = Console.ReadLine(); })
                                       .Add("LastName: " + person.LastName, () => { Console.Write("New Attribute: "); person.LastName = Console.ReadLine(); })
                                       .Add("Adress: " + person.Adress, () => { Console.Write("New Attribute: "); person.Adress = Console.ReadLine(); })
                                       .Add("PhoneNumber: " + person.PhoneNumber, () => { Console.Write("New Attribute: "); person.PhoneNumber = Console.ReadLine(); })
                                       .Add("Email: " + person.Email, () => { Console.Write("New Attribute: "); person.Email = Console.ReadLine(); })
                                       .Add("Save", ConsoleMenu.Close)
                                        .Configure(config =>
                                        {
                                            config.ClearConsole = true;
                                            config.Selector = "--> ";
                                            config.EnableFilter = false;
                                            config.Title = "Attribues ";
                                            config.EnableBreadcrumb = true;
                                            config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                        });
                                            
                                   subPersonMenu.Show();
                                   daraOffice.UpdatePersons(person);
                               })
                               .Add("New Bus", () =>
                               {
                                  Buses bus = new Buses();
                                  var subPersonMenu = new ConsoleMenu(args, 2)
                                       .Add("LicencePlate: " + bus.LicencePlate, () => { Console.Write("New Attribute: "); bus.LicencePlate = Console.ReadLine(); })
                                       .Add("Departures: " + bus.Departures.ToString(), () => {
                                           try { Console.WriteLine("yyy.MM.ddTHH:mm"); Console.Write("New Attribute: "); bus.Departures = DateTime.ParseExact(Console.ReadLine(), "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture); }
                                           catch (FormatException)
                                           {
                                               Console.WriteLine("Unable to parse");
                                           }
                                       })
                                       .Add("Arrival: " + bus.Arrival.ToString(), () => {
                                           try { Console.WriteLine("yyy.MM.ddTHH:mm"); Console.Write("New Attribute: "); bus.Arrival = DateTime.ParseExact(Console.ReadLine(), "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture); }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Capacity: " + bus.Capacity, () =>
                                       {
                                           Console.Write("New Attribute: ");
                                           try { bus.Capacity = Int32.Parse(Console.ReadLine()); }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("StopNumber: " + bus.StopNumber, () => {
                                           Console.Write("New Attribute: ");
                                           try { bus.StopNumber = Int32.Parse(Console.ReadLine()); }
                                           catch (FormatException)
                                           {
                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Save", ConsoleMenu.Close)
                                        .Configure(config =>
                                        {
                                            config.ClearConsole = true;
                                            config.Selector = "--> ";
                                            config.EnableFilter = false;
                                            config.Title = "Attributes";
                                            config.EnableBreadcrumb = false;
                                            config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                        });
                                   subPersonMenu.Show();
                                   daraOffice.UpdateBuses(bus);                                  
                               })
                               .Add("New Lodgings", () =>
                               {
                                       Lodgings lodging = new Lodgings();
                                       var subLodgingsMenu = new ConsoleMenu(args, 2)
                                       .Add("RoomNumber: " + lodging.RoomNumber, () => { Console.Write("New Attribute: "); lodging.RoomNumber = Console.ReadLine(); })
                                       .Add("Hotel: " + lodging.Hotel, () => { Console.Write("New Attribute: "); lodging.Hotel = Console.ReadLine(); })
                                       .Add("Quality: " + lodging.Quality, () => { Console.Write("New Attribute: "); lodging.Quality = Console.ReadLine(); })
                                       .Add("Space: " + lodging.Space, () => {
                                           Console.Write("New Attribute: ");
                                           try { lodging.Space = Int32.Parse(Console.ReadLine()); }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Price: " + lodging.Price, () => {
                                           Console.Write("New Attribute: ");
                                           try { lodging.Price = Int32.Parse(Console.ReadLine()); }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Save", ConsoleMenu.Close)
                                        .Configure(config =>
                                        {
                                            config.ClearConsole = true;
                                            config.Selector = "--> ";
                                            config.EnableFilter = false;
                                            config.Title = "Attributes";
                                            config.EnableBreadcrumb = false;
                                            config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                        });
                                       subLodgingsMenu.Show();
                                       daraOffice.UpdateLodgings(lodging);
                              })
                               .Add("New Travel", () =>
                               { 
                                       Travel travel = new Travel();
                                       var subLodgingsMenu = new ConsoleMenu(args, 2)
                                       .Add("Country: " + travel.Country, () => { Console.Write("New Attribute: "); travel.Country = Console.ReadLine(); })
                                       .Add("City: " + travel.City, () => { Console.Write("New Attribute: "); travel.City = Console.ReadLine(); })
                                       .Add("Starts: " + travel.Starts, () => {
                                           Console.Write("New Attribute: ");
                                           try { Console.WriteLine("yyyy.MM.ddTHH:mm"); travel.Starts = DateTime.ParseExact(Console.ReadLine(), "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture); }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Ends: " + travel.Ends, () => {
                                           Console.Write("New Attribute: ");
                                           try { Console.WriteLine("yyyy.MM.ddTHH:mm"); travel.Ends = DateTime.ParseExact(Console.ReadLine(), "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture); }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Price: " + travel.Price, () => {
                                           Console.Write("New Attribute: ");
                                           try { travel.Price = Int32.Parse(Console.ReadLine()); }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Save", ConsoleMenu.Close)
                                        .Configure(config =>
                                        {
                                            config.ClearConsole = true;
                                            config.Selector = "--> ";
                                            config.EnableFilter = false;
                                            config.Title = "Attributes";
                                            config.EnableBreadcrumb = false;
                                            config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                        });
                                   subLodgingsMenu.Show();
                                   daraOffice.UpdateTravels(travel);
                               })
                               .Add("New Booking", () =>
                               {        
                                       Bookings booking = new Bookings();
                                       var subBookingMenu = new ConsoleMenu(args, 2)
                                       .Add("Person:", () => {
                                           daraOffice.GetAllPerson().Select(x => $"{x.Id} {x.FirstName} {x.LastName}").ToConsole("Persons");
                                           try
                                           {
                                               Console.Write("Person ID: ");
                                               booking.PersonId = Int32.Parse(Console.ReadLine());
                                           }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Travel: ", () => {
                                           daraOffice.GetAllTravel().Select(x => $"{x.Id} {x.City} {x.Starts}").ToConsole("Travels");
                                           try
                                           {
                                               Console.Write("Travel ID: ");
                                               booking.TravelId = Int32.Parse(Console.ReadLine());
                                           }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Buses: ", () => {
                                           daraOffice.GetAllBuses().Select(x => $"{x.Id} {x.LicencePlate} {x.Departures}").ToConsole("Bueses");
                                           try
                                           {
                                               Console.Write("Bus ID: ");
                                               booking.BusId = Int32.Parse(Console.ReadLine());
                                           }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                       .Add("Lodgings: ", () => {
                                           daraOffice.GetAllLodgins().Select(x => $"{x.Id} {x.Hotel} {x.RoomNumber}").ToConsole("Lodgings");
                                           try
                                           {
                                               Console.Write("Lodging ID: ");
                                               booking.LodgingsId = Int32.Parse(Console.ReadLine());
                                           }
                                           catch (FormatException)
                                           {

                                               Console.WriteLine("Unable to parse");
                                               PressAnyKey();
                                           }
                                       })
                                      .Add("Seats on Bus: ", () => {
                                          Console.Write("Seats: ");
                                          booking.Seats = Console.ReadLine();
                                      })
                                       .Add("Save", ConsoleMenu.Close)
                                        .Configure(config =>
                                        {
                                            config.ClearConsole = true;
                                            config.Selector = "--> ";
                                            config.EnableFilter = false;
                                            config.Title = "Attributes";
                                            config.EnableBreadcrumb = false;
                                            config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                                        });
                                   subBookingMenu.Show();
                                   daraOffice.UpdateBooking(booking);
                               })
                         .Add("<Back", ConsoleMenu.Close)
                         .Configure(config =>
                         {
                             config.Selector = "--> ";
                             config.EnableFilter = true;
                             config.Title = "Delete ";
                             config.EnableBreadcrumb = true;
                             config.WriteBreadcrumbAction = titles => Console.WriteLine(string.Join(" / ", titles));
                         });

                var subGetOneMenu = new ConsoleMenu(args, 1)
                            .Add("Person", () => {
                                var subEntityMenu = new ConsoleMenu(args, level: 2)        
                                    .Configure(config =>
                                    {
                                        config.Selector = "--> ";
                                        config.EnableFilter = true;
                                        config.Title = "Get one";
                                        config.EnableWriteTitle = true;
                                        config.EnableBreadcrumb = true;
                                        config.ClearConsole = true;
                                    });                              
                                foreach (Persons x in daraOffice.GetAllPerson() ) {
                                    subEntityMenu.Add(" "+x.Id+" "+x.FirstName+" "+x.LastName, ()=> { Console.WriteLine($"Id, FirstName, LastName, Address, PhoneNumber, Email"); Console.WriteLine($"{x.Id},  {x.FirstName}, {x.LastName}, {x.Adress}, {x.PhoneNumber}, {x.Email}"); PressAnyKey(); });
                                }
                                subEntityMenu.Add("<Back", ConsoleMenu.Close);
                                subEntityMenu.Show();
                            })
                            .Add("Bus", () => {
                                var subEntityMenu = new ConsoleMenu(args, level: 2)
                                    .Configure(config =>
                                    {
                                        config.Selector = "--> ";
                                        config.EnableFilter = true;
                                        config.Title = "Get one";
                                        config.EnableWriteTitle = true;
                                        config.EnableBreadcrumb = true;
                                        config.ClearConsole = true;
                                    });
                                foreach (Buses x in daraOffice.GetAllBuses())
                                {
                                    subEntityMenu.Add(" " + x.Id + " " + x.LicencePlate + " " + x.Departures.ToString(), () => { Console.WriteLine("Id,LicencePlate,Departures,Arrival,Capacity,StopNumber"); Console.WriteLine($"{x.Id},{x.LicencePlate},{x.Departures},{x.Arrival},{x.Capacity},{x.StopNumber}"); PressAnyKey(); });
                                }
                                subEntityMenu.Add("<Back", ConsoleMenu.Close);
                                subEntityMenu.Show();
                            })
                            .Add("Lodgings", () => {
                                var subEntityMenu = new ConsoleMenu(args, level: 2)
                                    .Configure(config =>
                                    {
                                        config.Selector = "--> ";
                                        config.EnableFilter = true;
                                        config.Title = "Get one";
                                        config.EnableWriteTitle = true;
                                        config.EnableBreadcrumb = true;
                                        config.ClearConsole = true;
                                    });
                                foreach (Lodgings x in daraOffice.GetAllLodgins())
                                {
                                    subEntityMenu.Add(" " + x.Id + " " + x.Hotel + " " + x.RoomNumber, () => { Console.WriteLine("Id,RoomNumber,Hotel,Quality,Space,Price"); Console.WriteLine( $"{x.Id},{x.RoomNumber},{x.Hotel},{x.Quality},{x.Space},{x.Price}"); PressAnyKey(); });
                                }
                                subEntityMenu.Add("<Back", ConsoleMenu.Close);
                                subEntityMenu.Show();
                            })
                             .Add("Travel", () => {
                                 var subEntityMenu = new ConsoleMenu(args, level: 2)
                                     .Configure(config =>
                                     {
                                         config.Selector = "--> ";
                                         config.EnableFilter = true;
                                         config.Title = "Get one";
                                         config.EnableWriteTitle = true;
                                         config.EnableBreadcrumb = true;
                                         config.ClearConsole = true;
                                     });
                                 foreach (Travel x in daraOffice.GetAllTravel())
                                 {
                                     subEntityMenu.Add(" " + x.Id + " " + x.City + " " + x.Starts.ToString(), () => { Console.WriteLine("Id, Country, City, Starts, Ends, Price"); Console.WriteLine($"{x.Id},{x.Country},{x.City},{x.Starts},{x.Ends},{x.Price}"); PressAnyKey(); });
                                 }
                                 subEntityMenu.Add("<Back", ConsoleMenu.Close);
                                 subEntityMenu.Show();
                             })
                             .Add("Booking", () => {
                                 var subEntityMenu = new ConsoleMenu(args, level: 2)
                                     .Configure(config =>
                                     {
                                         config.Selector = "--> ";
                                         config.EnableFilter = true;
                                         config.Title = "Get one";
                                         config.EnableWriteTitle = true;
                                         config.EnableBreadcrumb = true;
                                         config.ClearConsole = true;
                                     });
                                 foreach (Bookings x in daraOffice.GetAllBookings())
                                 {
                                     subEntityMenu.Add(" " + x.Id + " " + x.Persons.LastName + " " + x.Travel.City+ " "+x.Travel.Starts.ToString(), () => { Console.WriteLine($"Id: Persons.FirstName,Persons.LastName \n  Buses.LicencePlate :Seats, \n  Travel.Country Travel.City, \n  Lodgings.Hotel: Lodgings.RoomNumber, \n  Travel.Price(HUF)"); Console.WriteLine( $"{x.Id}: {x.Persons.FirstName},{x.Persons.LastName} \n  {x.Buses.LicencePlate} : {x.Seats}, \n  {x.Travel.Country} {x.Travel.City}, \n  {x.Lodgings.Hotel}: {x.Lodgings.RoomNumber}, \n  {x.Travel.Price}"); PressAnyKey(); });
                                 }
                                 subEntityMenu.Add("<Back", ConsoleMenu.Close);
                                 subEntityMenu.Show();
                             })

                     .Add("<Back", ConsoleMenu.Close)
                     .Configure(config =>
                        {
                            config.Selector = "--> ";
                            config.EnableFilter = true;
                            config.Title = "Get one";
                            config.EnableWriteTitle = true;
                            config.EnableBreadcrumb = true;
                            config.ClearConsole = true;
                        });

   var menu = new ConsoleMenu(args, level: 0)
        .Add("Lists>", subListMenu.Show)
        .Add("Get One>", subGetOneMenu.Show)
        .Add("Update>",subUpdateMenu.Show)
        .Add("New>", subNewMenu.Show)
        .Add("Delete>", subDeleteMenu.Show)       
        .Add("Extra List>", subExtraMenu.Show)
        .Add("Extra Task List>", subExtraAsyncMenu.Show)
        .Add("Exit", () => Environment.Exit(0))
        .Configure(config =>
        {
            config.Selector = "--> ";
            config.EnableFilter = true;
            config.Title = "Dara Zrt. Main menu";
            config.EnableWriteTitle = true;
            config.EnableBreadcrumb = true;
            config.ClearConsole = true;
        });
                menu.Show();
                daraOffice.UpdateBuses(new Buses());
                
            }


        }    
        private static void WriteFromICollection(string muvelet,ICollection<dynamic> list)
        {
            Console.WriteLine(muvelet+":\n ");
            foreach (dynamic item in list)
            {
                Console.WriteLine(list.ToString()+"\n");
            }
            PressAnyKey();
        }
        private static void PressAnyKey()
        {        
            Console.WriteLine("****Press Any Key****");
            Console.ReadLine();
        }
        private static void SomeAction(string v)
        {
            throw new NotImplementedException();
        }
    }
}
