﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<NikGitStats>", Scope = "member", Target = "~P:Darazrt.Web.Models.PersonListViewModel.ListOfPersons")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:Darazrt.Web.Controllers.PersonsController.#ctor(DaraZrt.Logic.IDaraOffice,AutoMapper.IMapper)")]
[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1309:Field names should not begin with underscore", Justification = "<NikGitStats>", Scope = "member", Target = "~F:Darazrt.Web.Controllers.HomeController._logger")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<NikGitStats>", Scope = "member", Target = "~P:Darazrt.Web.Models.PersonListViewModel.ListOfPersons")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]