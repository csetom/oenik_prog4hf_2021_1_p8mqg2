﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Darazrt.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// MapperFactory.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Crate Mapper.
        /// </summary>
        /// <returns>Mapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DaraZrt.Data.Models.Persons, Web.Models.Persons>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).// Elvileg ez nem kell nekem, de azert beleirjuk.
                    ForMember(dest => dest.LastName, map => map.MapFrom(src => src.LastName)).
                    ForMember(dest => dest.FirstName, map => map.MapFrom(src => src.FirstName)).
                    ForMember(dest => dest.Adress, map => map.MapFrom(src => src.Adress)).
                    ForMember(dest => dest.PhoneNumber, map => map.MapFrom(src => src.PhoneNumber)).
                    ForMember(dest => dest.Email, map => map.MapFrom(src => src.Email)).
                    ReverseMap();
            });

            return config.CreateMapper();
        }
    }
}
