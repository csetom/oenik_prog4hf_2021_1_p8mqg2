﻿// <copyright file="Persons.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Darazrt.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Persons Model.
    /// </summary>
    public class Persons
    {
        /// <summary>
        /// Gets or sets person Id.
        /// </summary>
        [Display(Name = "Person Id")]
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets keresztnev.
        /// </summary>
        [Display(Name = "Persons first name")]
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets vezeteknev.
        /// </summary>
        [Display(Name = "Persons Last name")]
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets lakcim.
        /// </summary>
        [Display(Name = "Persons Address")]
        public string Adress { get; set; }

        /// <summary>
        /// Gets or sets telefon.
        /// </summary>
        [Display(Name = "Person Phone Number")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets email.
        /// </summary>
        [Display(Name = "Person Email")]
        public string Email { get; set; }
    }
}
