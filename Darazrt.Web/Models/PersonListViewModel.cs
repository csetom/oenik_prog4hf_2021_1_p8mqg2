﻿// <copyright file="PersonListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Darazrt.Web.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// ListView Model.
    /// </summary>
    public class PersonListViewModel
    {
        /// <summary>
        /// Gets or sets list of persons.
        /// </summary>
        public List<Persons> ListOfPersons { get; set; }

        /// <summary>
        /// Gets or sets editedPersons.
        /// </summary>
        public Persons EditedPerson { get; set; }
    }
}
