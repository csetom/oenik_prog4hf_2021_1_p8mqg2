﻿// <copyright file="PersonsController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Darazrt.Web.Controllers
{
    using System.Collections.Generic;
    using AutoMapper;
    using DaraZrt.Logic;
    using Darazrt.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The controller.
    /// </summary>
    public class PersonsController : Controller
    {
        private IDaraOffice logic;
        private IMapper mapper;
        private PersonListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonsController"/> class.
        /// </summary>
        /// <param name="logic">DaraOffice logic.</param>
        /// <param name="mapper">Mapper.</param>
        public PersonsController(IDaraOffice logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            vm = new PersonListViewModel();
            vm.EditedPerson = new Models.Persons();

            var persons = logic.GetAllPerson();
            vm.ListOfPersons = mapper.Map<ICollection<DaraZrt.Data.Models.Persons>, List<Models.Persons>>(persons);
        }

        /// <summary>
        /// GET index page.
        /// </summary>
        /// <returns>View page.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("PersonIndex", this.vm);
        }

        /// <summary>
        /// Details.
        /// </summary>
        /// <param name="id"> Persons Id. </param>
        /// <returns>View Page with Deatils.</returns>
        public IActionResult Details(int id)
        {
            return View("PersonDetails", GetPersonModel(id));
        }

        /// <summary>
        /// Remove Persons.
        /// </summary>
        /// <param name="id"> Person id.</param>
        /// <returns>Action. </returns>
        public IActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete Fail";
            logic.DeletePersons(logic.GetPerson(id));
            TempData["editResult"] = "Delet OK";
            return RedirectToAction(nameof(Index)); // View("PersonRemove", GetPersonModel(id));
        }

        /// <summary>
        /// Get EDIT.
        /// </summary>
        /// <param name="id">Persons Id.</param>
        /// <returns>View.</returns>
        public IActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedPerson = GetPersonModel(id);
            return View("PersonIndex", vm);
        }

        /// <summary>
        /// Posting the editing.
        /// </summary>
        /// <param name="person">Person.</param>
        /// <param name="editAction">EditAction.</param>
        /// <returns>PersonIndex.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Persons person, string editAction)
        {
            if (ModelState.IsValid && person != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.UpdatePersons(new DaraZrt.Data.Models.Persons() { LastName = person.LastName, FirstName = person.FirstName, Adress = person.Adress, Email = person.Email, PhoneNumber = person.PhoneNumber });
                }
                else
                {
                    logic.UpdatePersons(new DaraZrt.Data.Models.Persons() { Id = person.Id, LastName = person.LastName, FirstName = person.FirstName, Adress = person.Adress, Email = person.Email, PhoneNumber = person.PhoneNumber });
                }

                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedPerson = person;
                return View("PersonIndex", vm);
            }
        }

        private Models.Persons GetPersonModel(int id)
        {
            DaraZrt.Data.Models.Persons onePerson = this.logic.GetPerson(id);
            return this.mapper.Map<DaraZrt.Data.Models.Persons, Models.Persons>(onePerson);
        }
    }
}
