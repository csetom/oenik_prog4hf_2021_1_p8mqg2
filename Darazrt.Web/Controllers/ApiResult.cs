﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Darazrt.Web.Controllers
{
    /// <summary>
    /// Api Result.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether operationResult.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
