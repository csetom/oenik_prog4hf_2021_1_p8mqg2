﻿// <copyright file="PersonsApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Darazrt.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using DaraZrt.Data.Models;
    using DaraZrt.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Api Controller.
    /// </summary>
    public partial class PersonsApiController : Controller
    {
        private IDaraOffice logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonsApiController"/> class.
        /// </summary>
        /// <param name="logic">Logic.</param>
        /// <param name="mapper">Mapper.</param>
        public PersonsApiController(IDaraOffice logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// GET PersonsApi/all.
        /// </summary>
        /// <returns>Everything.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Persons> GetAll()
        {
            var persons = logic.GetAllPerson();
            return mapper.Map<IEnumerable<DaraZrt.Data.Models.Persons>, List<Models.Persons>>(persons);
        }

        /// <summary>
        /// GET PersonsApi/del/5.
        /// </summary>
        /// <param name="id">Person id.</param>
        /// <returns>ApiResult.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOnePerson(int id)
        {
            logic.DeletePersons(logic.GetPerson(id));
            return new ApiResult() { OperationResult = true };
        }

        /// <summary>
        /// // POST CarsApi/add + Post.
        /// </summary>
        /// <param name="persons">Persons Data.</param>
        /// <returns>Api Result.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOnePerson(Models.Persons persons)
        {
            bool succses = true;
            if (persons == null)
            {
                succses = false;
            }
            else
            {
                try
                {
                    logic.UpdatePersons(new Persons()
                    {
                        Adress = persons.Adress,
                        Email = persons.Email,
                        FirstName = persons.FirstName,
                        LastName = persons.LastName,
                        PhoneNumber = persons.PhoneNumber,
                    });
                }
                catch (ArgumentException)
                {
                    succses = false;
                }
            }

            return new ApiResult()
            {
                OperationResult = succses,
            };
        }

        /// <summary>
        /// POST CarsApi/mod + Post.
        /// </summary>
        /// <param name="persons">Persons Data.</param>
        /// <returns>Api Result.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOnePerson(Models.Persons persons)
        {
            bool succses = true;
            if (persons == null)
            {
                succses = false;
            }
            else
            {
                try
                {
                    logic.UpdatePersons(new Persons()
                    {
                        Id = persons.Id,
                        Adress = persons.Adress,
                        Email = persons.Email,
                        FirstName = persons.FirstName,
                        LastName = persons.LastName,
                        PhoneNumber = persons.PhoneNumber,
                    });
                }
                catch (ArgumentException)
                {
                    succses = false;
                }
            }

            return new ApiResult()
            {
                OperationResult = succses,
            };
        }
    }
}
