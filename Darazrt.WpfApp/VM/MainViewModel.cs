﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfApp.VM
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using DaraZrt.Data;
    using DaraZrt.Logic;
    using DaraZrt.Repository;
    using DaraZrt.WpfApp.BL;
    using DaraZrt.WpfApp.Data;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// Main View Model.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IPersonLogic logic;
        private Person personSelected;
        private ICommand addCmd;
        private ICommand modCmd;
        private ICommand delCmd;

        /// <summary>
        /// Gets observableCollection.
        /// </summary>
        public ObservableCollection<Person> Persons { get; private set; }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public Person PersonSelected { get => personSelected; set => Set(ref personSelected, value); }

        /// <summary>
        /// Gets get Add Command.
        /// </summary>
        /// <returns> Add command.</returns>
        public ICommand AddCmd => addCmd;

        /// <summary>
        /// Set Add Command.
        /// </summary>
        /// <param name="value">Add command.</param>
        private void SetAddCmd(ICommand value)
        {
            addCmd = value;
        }

        /// <summary>
        /// Gets get Mod Command.
        /// </summary>
        /// <returns>Mod Command.</returns>
        public ICommand ModCmd => modCmd;

        /// <summary>
        /// Set Mod Command.
        /// </summary>
        /// <param name="value">Mod command.</param>
        private void SetModCmd(ICommand value)
        {
            modCmd = value;
        }

        /// <summary>
        /// Gets get Del Command.
        /// </summary>
        /// <returns>Del Command.</returns>
        public ICommand DelCmd => delCmd;

        /// <summary>
        /// Set Del Command.
        /// </summary>
        /// <param name="value">Del command.</param>
        private void SetDelCmd(ICommand value)
        {
            delCmd = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">Logika.</param>
        public MainViewModel(IPersonLogic logic)
        {
            if (IsInDesignMode)
            {
                return;
            }

            this.logic = logic;
            Persons = new ObservableCollection<Person>();

            foreach (var elm in this.logic.GetAllPerson())
            {
                Persons.Add(elm);
            }

            if (IsInDesignMode)
            {
                Person p1 = new Person()
                {
                    FirstName = "Feri",
                    LastName = "Teszt",
                    Adress = "Teszt utca 2",
                    Email = "teszt@teszt.hu",
                    PhoneNumber = "123456",
                };
                Persons.Add(p1);
            }

            SetAddCmd(new RelayCommand(() => this.logic.AddPerson(Persons)));
            SetModCmd(new RelayCommand(() => this.logic.ModPerson(personSelected)));
            SetDelCmd(new RelayCommand(() => this.logic.DelPerson(Persons, personSelected)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IPersonLogic>())
        {
        }
    }
}
