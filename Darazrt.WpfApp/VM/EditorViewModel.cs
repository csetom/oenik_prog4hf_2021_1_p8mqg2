﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DaraZrt.WpfApp.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Editor View Model.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        /// <summary>
        /// .
        /// </summary>
        private Person person;

        /// <summary>
        /// Gets.
        /// </summary>
        public static Array Positions
        {
            get { return Enum.GetValues(typeof(PositionType)); }
        }

        /// <summary>
        /// Gets.
        /// </summary>
        public static Array Statuses
        {
            get { return Enum.GetValues(typeof(StatusType)); }
        }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public Person Person { get => person; set => Set(ref person, value); }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            person = new Person();
            if (IsInDesignMode)
            {
                person.FirstName = "Elek";
                person.LastName = "Teszt";
                person.Adress = "Teszt uca 2";
                person.PhoneNumber = "06123456";
                person.Email = "teszt@teszt.hu";
            }
        }
    }
}
