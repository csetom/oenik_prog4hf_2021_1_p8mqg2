﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfApp
{
    using System.Windows;
    using CommonServiceLocator;
    using DaraZrt.Data;
    using DaraZrt.Logic;
    using DaraZrt.Repository;
    using DaraZrt.WpfApp.BL;
    using DaraZrt.WpfApp.UI;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <inheritdoc/>
        private class MyIoc : SimpleIoc, IServiceLocator
        {
            /// <summary>
            ///  Gets instance.
            /// </summary>
            public static MyIoc Instance { get; private set; } = new MyIoc();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            if (!SimpleIoc.Default.IsRegistered<IDaraOffice>())
            {
                DaraDBContext ctx = new DaraDBContext();
                DaraLogic daraLogic = new DaraLogic(
                    new BookingsRepository(ctx),
                    new PersonsRepository(ctx),
                    new BusesRepository(ctx),
                    new TravelRepository(ctx),
                    new LodgingsRepository(ctx));

                DaraOffice daraOffice = daraLogic.DaraOffice;
                MyIoc.Instance.Register<IDaraOffice>(() => daraOffice);
            }

            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);
            MyIoc.Instance.Register<IEditorService, EditorServiceViaWindow>();
            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIoc.Instance.Register<IPersonLogic, PersonLogic>();
        }
    }
}
