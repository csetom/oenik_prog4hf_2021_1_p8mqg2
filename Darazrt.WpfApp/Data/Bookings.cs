﻿// <copyright file="Bookings.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

        /// <summary>
        /// Bookings.
        /// </summary>
    public class Bookings
        {
            /// <summary>
            /// Gets or sets id.
            /// </summary>
            public int Id { get; set; }

            /// <summary>
            /// Gets or sets busId.
            /// </summary>
            public int BusId { get; set; }

            /// <summary>
            /// Gets or sets utasId.
            /// </summary>
            public int PersonId { get; set; }

            /// <summary>
            /// Gets or sets ulohely.
            /// </summary>
            public string Seats { get; set; }

            /// <summary>
            ///  Gets or sets datum.
            /// </summary>
            public DateTime Date { get; set; } // Foglalasi datum

            /// <summary>
            ///  Gets or sets szallasid.
            /// </summary>
            public int LodgingsId { get; set; }

            /// <summary>
            ///  Gets or sets utazasid.
            /// </summary>
            public int TravelId { get; set; }

            /// <inheritdoc/>
            public override bool Equals(object obj)
            {
                if (obj is Bookings)
                {
                    Bookings other = obj as Bookings;
                    return this.Id == other.Id &&
                            this.BusId == other.BusId &&
                            this.LodgingsId == other.LodgingsId &&
                            this.PersonId == other.PersonId &&
                            this.TravelId == other.TravelId &&
                            this.Seats == other.Seats;
                }
                else
                {
                    return false;
                }
            }

            /// <inheritdoc/>
            public override int GetHashCode()
            {
                return this.Id;
            }

            /// <inheritdoc/>
            public override string ToString()
            {
                return $"{this.Id},{this.BusId}, {this.LodgingsId}, {this.PersonId}, {this.TravelId}, {this.Seats},";
            }
    }
}
