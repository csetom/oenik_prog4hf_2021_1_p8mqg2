﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfApp.Data
{
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Not In use.
    /// </summary>
    public enum PositionType
    {
       /// <inheritdoc/>
        None,
    }

    /// <summary>
    /// Not in use.
    /// </summary>
    public enum StatusType
    {
        /// <inheritdoc/>
        None,
    }

    /// <summary>
    /// Person Main Data.
    /// </summary>
    public class Person : ObservableObject
    {
        private int id;
        private string firstName;
        private string lastName;
        private string adress;
        private string phoneNumber;
        private string email;

        /// <summary>
        /// Gets or sets id get,set.
        /// </summary>
        public int Id { get => id; set => Set(ref id, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string FirstName { get => firstName; set => Set(ref firstName, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string LastName { get => lastName; set => Set(ref lastName, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string Adress { get => adress; set => Set(ref adress, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string PhoneNumber { get => phoneNumber; set => Set(ref phoneNumber, value); }

        /// <summary>
        /// Gets or sets.
        /// </summary>
        public string Email { get => email; set => Set(ref email, value); }

        /// <summary>
        /// Copy Person.
        /// </summary>
        /// <param name="other">The other Person.</param>
        public void CopyFrom(Person other)
        {
            GetType().GetProperties().ToList().
                ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
