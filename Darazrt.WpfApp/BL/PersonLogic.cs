﻿// <copyright file="PersonLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfApp.BL
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DaraZrt.Data.Models;
    using DaraZrt.Logic;
    using DaraZrt.WpfApp.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Person Logic.
    /// </summary>
    public class PersonLogic : IPersonLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private IDaraOffice daraOffice;

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonLogic"/> class.
        /// </summary>
        /// <param name="editorService">Editor Service.</param>
        /// <param name="messengerService">Messenger Service.</param>
        /// <param name="daraOffice">DaraOffice.</param>
        public PersonLogic(IEditorService editorService, IMessenger messengerService, IDaraOffice daraOffice)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.daraOffice = daraOffice;
        }

        /// <summary>
        /// Add new Person to list.
        /// </summary>
        /// <param name="list">The list of persons.</param>
        public void AddPerson(IList<Person> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list), "List is null");
            }

            Person newPerson = new Person();
            if (editorService.EditPerson(newPerson) == true)
            {
                Persons modelPerson = new Persons()
                {
                    Email = newPerson.Email,
                    FirstName = newPerson.FirstName,
                    LastName = newPerson.LastName,
                    PhoneNumber = newPerson.PhoneNumber,
                    Adress = newPerson.Adress,
                };

                newPerson.Id = daraOffice.AddPersons(modelPerson);
                modelPerson = null;
                list.Add(newPerson);
                messengerService.Send("Add ok", "LogicResult");
            }
            else
            {
                messengerService.Send("Add Cancel", "LogicResult");
            }
        }

        /// <summary>
        /// Delete Person.
        /// </summary>
        /// <param name="list">The List of persons.</param>
        /// <param name="person">The person I want to delete.</param>
        public void DelPerson(IList<Person> list, Person person)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list), "List is null");
            }

            if (person != null)
            {
                Persons modelPerson = new Persons()
                {
                    Id = person.Id,
                    Email = person.Email,
                    FirstName = person.FirstName,
                    LastName = person.LastName,
                    PhoneNumber = person.PhoneNumber,
                    Adress = person.Adress,
                };
                daraOffice.DeletePersons(daraOffice.GetPerson(modelPerson.Id));
                if (list.Remove(person))
                {
                    messengerService.Send("Delete ok", "LogicResult");
                }
                else
                {
                    messengerService.Send("Delete Faild", "LogicResult");
                }
            }
            else
            {
                messengerService.Send("Delete Faild", "LogicResult");
            }
        }

        /// <summary>
        /// Get All Person.
        /// </summary>
        /// <returns>All the person from DB.</returns>
        public IList<Person> GetAllPerson()
        {
            IList<Person> list = new List<Person>();

            foreach (Persons elm in daraOffice.GetAllPerson())
            {
                Person person = new Person()
                {
                    Id = elm.Id,
                    Email = elm.Email,
                    FirstName = elm.FirstName,
                    LastName = elm.LastName,
                    PhoneNumber = elm.PhoneNumber,
                    Adress = elm.Adress,
                };
                list.Add(person);
            }

            return list;
        }

        /// <summary>
        /// Person Modifing.
        /// </summary>
        /// <param name="personToModify">The Person I want to Modify.</param>
        public void ModPerson(Person personToModify)
        {
            if (personToModify == null)
            {
                messengerService.Send("Edit Faild", "LogicResult");
                return;
            }

            Person clone = new Person();
            clone.CopyFrom(personToModify);
            if (editorService.EditPerson(clone) == true)
            {
                Persons modelPerson = new Persons()
                {
                    Id = clone.Id,
                    Email = clone.Email,
                    FirstName = clone.FirstName,
                    LastName = clone.LastName,
                    PhoneNumber = clone.PhoneNumber,
                    Adress = clone.Adress,
                };
                daraOffice.UpdatePersons(modelPerson);
                personToModify.CopyFrom(clone);
                modelPerson = null;
                messengerService.Send("Modify ok", "LogicResult");
            }
            else
            {
                messengerService.Send("Modify cancel", "LogicResult");
            }
        }
    }
}
