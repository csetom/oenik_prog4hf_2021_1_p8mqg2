﻿// <copyright file="IPersonLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DaraZrt.WpfApp.Data;

    /// <summary>
    /// Person Logic Interafce.
    /// </summary>
    public interface IPersonLogic
    {
        /// <summary>
        /// Add new Person.
        /// </summary>
        /// <param name="list">The list of persons.</param>
        void AddPerson(IList<Person> list);

        /// <summary>
        /// Person Modifying.
        /// </summary>
        /// <param name="personToModify">Person that I want to modify.</param>
        void ModPerson(Person personToModify);

        /// <summary>
        /// Delete Person.
        /// </summary>
        /// <param name="list">Persons.</param>
        /// <param name="person">The Person I want to delete.</param>
        void DelPerson(IList<Person> list, Person person);

        /// <summary>
        /// Get All Person.
        /// </summary>
        /// <returns>All persons.</returns>
        IList<Person> GetAllPerson(); // Todo
    }
}
