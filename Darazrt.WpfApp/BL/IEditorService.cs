﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfApp.BL
{
    using DaraZrt.WpfApp.Data;

    /// <summary>
    /// Editor Service Interface.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Editable Person.
        /// </summary>
        /// <param name="p">Person.</param>
        /// <returns>Its editable or not.</returns>
        bool EditPerson(Person p);
    }
}
