﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.WpfApp.UI
{
    using DaraZrt.WpfApp.BL;
    using DaraZrt.WpfApp.Data;

    /// <summary>
    /// Editor Service Via Window.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// Editing the Person.
        /// </summary>
        /// <param name="p">The edited person.</param>
        /// <returns>Dialog window.</returns>
        public bool EditPerson(Person p)
        {
            EditorWindow win = new EditorWindow(p);
            return win.ShowDialog() ?? false;
        }
    }
}
