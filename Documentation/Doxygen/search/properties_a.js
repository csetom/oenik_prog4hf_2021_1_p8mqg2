var searchData=
[
  ['personid_251',['PersonId',['../class_dara_zrt_1_1_data_1_1_models_1_1_bookings.html#a8a9b0b8347081cadddb6d5dae10a9431',1,'DaraZrt::Data::Models::Bookings']]],
  ['persons_252',['Persons',['../class_dara_zrt_1_1_data_1_1_dara_d_b_context.html#a4ee375cebf6b8af4fb3d55a2cb1fb01c',1,'DaraZrt.Data.DaraDBContext.Persons()'],['../class_dara_zrt_1_1_data_1_1_models_1_1_bookings.html#afe84c791c7acb472881d45427aa86822',1,'DaraZrt.Data.Models.Bookings.Persons()']]],
  ['phonenumber_253',['PhoneNumber',['../class_dara_zrt_1_1_data_1_1_models_1_1_persons.html#a4028485815d8aadc3f1bb336d3a0ed67',1,'DaraZrt::Data::Models::Persons']]],
  ['price_254',['Price',['../class_dara_zrt_1_1_data_1_1_models_1_1_lodgings.html#a7811a3b9657586749f8318bb0f88515e',1,'DaraZrt.Data.Models.Lodgings.Price()'],['../class_dara_zrt_1_1_data_1_1_models_1_1_travel.html#ae45efab7b83f9fd06bf71d232419186e',1,'DaraZrt.Data.Models.Travel.Price()'],['../class_dara_zrt_1_1_logic_1_1_models_1_1_cheap_booking.html#ab6bb343c79aecb239ea151a2cac7a578',1,'DaraZrt.Logic.Models.CheapBooking.Price()']]]
];
