var searchData=
[
  ['lastname_245',['LastName',['../class_dara_zrt_1_1_data_1_1_models_1_1_persons.html#abf4bc3d0e70995d3d755b886ce56363c',1,'DaraZrt.Data.Models.Persons.LastName()'],['../class_dara_zrt_1_1_logic_1_1_models_1_1_summer_people.html#ad2e64aeee0658c8ddccdc62b93fa644e',1,'DaraZrt.Logic.Models.SummerPeople.LastName()']]],
  ['licenceplate_246',['LicencePlate',['../class_dara_zrt_1_1_data_1_1_models_1_1_buses.html#a15522680fa3576f65f434268fbcb32d2',1,'DaraZrt.Data.Models.Buses.LicencePlate()'],['../class_dara_zrt_1_1_logic_1_1_models_1_1_bookable_busses.html#ad1e78f3bc902c8cd4f9404d373a7d28a',1,'DaraZrt.Logic.Models.BookableBusses.LicencePlate()']]],
  ['lodgings_247',['Lodgings',['../class_dara_zrt_1_1_data_1_1_dara_d_b_context.html#a5fd564525c008d81abd6407a5e17bca5',1,'DaraZrt.Data.DaraDBContext.Lodgings()'],['../class_dara_zrt_1_1_data_1_1_models_1_1_bookings.html#ae73a3705e9c22adf2cd1e8472b45e4f8',1,'DaraZrt.Data.Models.Bookings.Lodgings()']]],
  ['lodgingsid_248',['LodgingsId',['../class_dara_zrt_1_1_data_1_1_models_1_1_bookings.html#a28da88dc797a5f4bb357fac1d195214b',1,'DaraZrt::Data::Models::Bookings']]]
];
