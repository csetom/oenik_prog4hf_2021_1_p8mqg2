﻿// <copyright file="DaraOffice.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DaraZrt.Data.Models;
    using DaraZrt.Logic.Models;
    using DaraZrt.Repository;

    /// <summary>
    ///  DaraIdora.
    /// </summary>
    public class DaraOffice : IDaraOffice
    {
        private readonly IRepository<Bookings> bookingsrepository;
        private readonly IRepository<Persons> personsrepository;
        private readonly IRepository<Buses> busesrepository;
        private readonly IRepository<Travel> travelrepository;
        private readonly IRepository<Lodgings> lodgingsrepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="DaraOffice"/> class.
        /// </summary>
        /// <param name="bookingrepository">Foglalas repo.</param>
        /// <param name="personsrepository">Utas repo.</param>
        /// <param name="busesrepository">Inditott busz repo.</param>
        /// <param name="travelrepository">Travel repo.</param>
        /// <param name="lodgingsrepository">Szallas repo.</param>
        public DaraOffice(IRepository<Bookings> bookingrepository, IRepository<Persons> personsrepository, IRepository<Buses> busesrepository, IRepository<Travel> travelrepository, IRepository<Lodgings> lodgingsrepository)
        {
            this.bookingsrepository = bookingrepository;
            this.personsrepository = personsrepository;
            this.busesrepository = busesrepository;
            this.travelrepository = travelrepository;
            this.lodgingsrepository = lodgingsrepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DaraOffice"/> class.
        /// </summary>
        /// <param name="bookingrepository">Booking Repo. </param>
        /// <param name="personsrepository">Persons Repo. </param>
        /// <param name="busesrepository">Bus Repo.</param>
        /// <param name="travelrepository">Travel Repo. </param>
        /// <param name="lodgingsrepository">Lodging Repo. </param>
        public DaraOffice(IBookingsRepository bookingrepository, IPersonsRepository personsrepository, IBusesRepository busesrepository, ITravelRepository travelrepository, ILodgingsRepository lodgingsrepository)
        {
            this.bookingsrepository = bookingrepository;
            this.personsrepository = personsrepository;
            this.busesrepository = busesrepository;
            this.travelrepository = travelrepository;
            this.lodgingsrepository = lodgingsrepository;
        }

        /// <summary>
        /// GeAllBookings.
        /// </summary>
        /// <returns> Bookings.</returns>
        public ICollection<Bookings> GetAllBookings()
        {
            return this.bookingsrepository.GetAll().ToList();
        }

        /// <summary>
        /// GetAllPersons.
        /// </summary>
        /// <returns> Osszes utas.</returns>
        public ICollection<Persons> GetAllPerson()
        {
            return this.personsrepository.GetAll().ToList();
        }

        /// <summary>
        /// GetAllBuses.
        /// </summary>
        /// <returns>Inditott buszok.</returns>
        public ICollection<Buses> GetAllBuses()
        {
            return this.busesrepository.GetAll().ToList();
        }

        /// <summary>
        /// GetAllLodgins.
        /// </summary>
        /// <returns>Osszes szallas.</returns>
        public ICollection<Lodgings> GetAllLodgins()
        {
            return this.lodgingsrepository.GetAll().ToList();
        }

        /// <summary>
        /// GetAllTravel.
        /// </summary>
        /// <returns>Osszes Travel.</returns>
        public ICollection<Travel> GetAllTravel()
        {
            return this.travelrepository.GetAll().ToList();
        }

        /// <summary>
        /// UpdateTravels.
        /// </summary>
        /// <param name="entity">Egy utazas.</param>
        public void UpdateTravels(Travel entity)
        {
            this.travelrepository.Update(entity);
            this.travelrepository.Commit();
        }

        /// <summary>
        /// UpdateLodgings.
        /// </summary>
        /// <param name="entity">Egy szallas.</param>
        public void UpdateLodgings(Lodgings entity)
        {
            this.lodgingsrepository.Update(entity);
            this.lodgingsrepository.Commit();
        }

        /// <summary>
        /// UpdateBuses.
        /// </summary>
        /// <param name="entity">Busz entity.</param>
        public void UpdateBuses(Buses entity)
        {
            this.busesrepository.Update(entity);
            this.busesrepository.Commit();
        }

        /// <summary>
        /// UpdateBooking.
        /// </summary>
        /// <param name="entity">Bookings entity.</param>
        public void UpdateBooking(Bookings entity)
        {
            this.bookingsrepository.Update(entity);
            this.bookingsrepository.Commit();
        }

        /// <summary>
        /// UpdatePersons.
        /// </summary>
        /// <param name="entity">Utas entity.</param>
        public void UpdatePersons(Persons entity)
        {
            this.personsrepository.Update(entity);
            this.personsrepository.Commit();
        }

        /// <summary>
        /// AddPersons.
        /// </summary>
        /// <param name="entity">Utas entity.</param>
        /// <returns>Id.</returns>
        public int AddPersons(Persons entity)
        {
           int id = this.personsrepository.Add(entity);
           this.personsrepository.Commit();
           return id;
        }

        /// <summary>
        /// DeleteTravel.
        /// </summary>
        /// <param name="entity">Travel entity.</param>
        public void DeleteTravel(Travel entity)
        {
            this.travelrepository.Delete(entity);
            this.travelrepository.Commit();
        }

        /// <summary>
        /// DeleteLodgings.
        /// </summary>
        /// <param name="entity">Szallas entity.</param>
        public void DeleteLodgings(Lodgings entity)
        {
            this.lodgingsrepository.Delete(entity);
            this.lodgingsrepository.Commit();
        }

        /// <summary>
        /// DeleteBuses.
        /// </summary>
        /// <param name="entity">Busses entity.</param>
        public void DeleteBuses(Buses entity)
        {
            this.busesrepository.Delete(entity);
            this.busesrepository.Commit();
        }

        /// <summary>
        /// DeleteUtfoglalas.
        /// </summary>
        /// <param name="entity">Bookings entity.</param>
        public void DeleteBooking(Bookings entity)
        {
            this.bookingsrepository.Delete(entity);
            this.bookingsrepository.Commit();
        }

        /// <summary>
        /// DeletePersons.
        /// </summary>
        /// <param name="entity">Utas entity.</param>
        public void DeletePersons(Persons entity)
        {
            this.personsrepository.Delete(entity);
            this.personsrepository.Commit();
        }

        /// <summary>
        /// Get One Person.
        /// </summary>
        /// <param name="id">Person Id.</param>
        /// <returns>Person.</returns>
        public Persons GetPerson(int id)
        {
            return this.personsrepository.GetByID(id);
        }

        /// <summary>
        /// Get One Bus.
        /// </summary>
        /// <param name="id">Bus Id.</param>
        /// <returns>Bus.</returns>
        public Buses GetBus(int id)
        {
            return this.busesrepository.GetByID(id);
        }

        /// <summary>
        /// Get One Lodging.
        /// </summary>
        /// <param name="id">Lodging Id.</param>
        /// <returns>Lodging.</returns>
        public Lodgings GetLodging(int id)
        {
            return this.lodgingsrepository.GetByID(id);
        }

        /// <summary>
        /// Get One Booking By Id.
        /// </summary>
        /// <param name="id"> Booking Id.</param>
        /// <returns>Booking.</returns>
        public Bookings GetBooking(int id)
        {
            return this.bookingsrepository.GetByID(id);
        }

        /// <summary>
        /// Get One Travel By id.
        /// </summary>
        /// <param name="id">Travel Id.</param>
        /// <returns>One Travel.</returns>
        public Travel GetTravel(int id)
        {
            return this.travelrepository.GetByID(id);
        }
    }
}
