﻿// <copyright file="SummerPeople.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// People who booked for summer.
    /// </summary>
    public class SummerPeople
    {
        /// <summary>
        /// Gets or sets start.
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// Gets or sets End.
        /// </summary>
        public DateTime End { get; set; }

        /// <summary>
        /// Gets or sets id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.Id}, {this.FirstName}, {this.LastName}, {this.Start}, {this.End}, {this.City}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is SummerPeople)
            {
                SummerPeople other = obj as SummerPeople;
                return this.Id == other.Id &&
                        this.Start == other.Start &&
                        this.End == other.End &&
                        this.City == other.City &&
                         this.LastName == other.LastName &&
                        this.FirstName == other.FirstName;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Id;
        }
    }
}
