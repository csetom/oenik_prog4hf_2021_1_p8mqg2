﻿// <copyright file="BookableBusses.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic.Models
{
    /// <summary>
    /// Foglalhato buszok.
    /// </summary>
    public class BookableBusses
    {
        /// <summary>
        /// Gets or sets id.
        /// id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets LicencePlate.
        /// </summary>
        public string LicencePlate { get; set; }

        /// <summary>
        /// Gets or sets FreeSteats.
        /// </summary>
        public int NumberOfFreeSeats { get; set; }

        /// <summary>
        /// Gets or sets numberOfSeats.
        /// </summary>
        public int NumberOfSeats { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $" Busz: {this.Id} {this.LicencePlate} number of free seats: {this.NumberOfFreeSeats}, {this.NumberOfSeats}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is BookableBusses)
            {
                BookableBusses other = obj as BookableBusses;
                return this.Id == other.Id &&
                        this.LicencePlate == other.LicencePlate &&
                        this.NumberOfFreeSeats == other.NumberOfFreeSeats &&
                        this.NumberOfSeats == other.NumberOfSeats;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Id;
        }
    }
}
