﻿// <copyright file="CheapBooking.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic.Models
{
    using System;

    /// <summary>
    /// Cheap Booking.
    /// </summary>
    public class CheapBooking
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets quality.
        /// </summary>
        public string Quality { get; set; }

        /// <summary>
        /// Gets or sets Hotel.
        /// </summary>
        public string Hotel { get; set; }

        /// <summary>
        /// Gets or sets Starts.
        /// </summary>
        public DateTime Starts { get; set; }

        /// <summary>
        /// Gets or sets Ends.
        /// </summary>
        public DateTime Ends { get; set; }

        /// <summary>
        /// Gets or sets Country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets Price.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// ToString.
        /// </summary>
        /// <returns> String.</returns>
        public override string ToString()
        {
            return $" {this.Id} Starts: {this.Starts}, Ends: {this.Ends}, Country:{this.Country}, City: {this.City}, Price: {this.Price} ";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is CheapBooking)
            {
                CheapBooking other = obj as CheapBooking;
                return this.Id == other.Id &&
                        this.Starts == other.Starts &&
                        this.Ends == other.Ends &&
                        this.Country == other.Country &&
                        this.City == other.City &&
                        this.Price == other.Price;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Id;
        }
    }
}
