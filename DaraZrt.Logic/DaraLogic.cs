﻿// <copyright file="DaraLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DaraZrt.Data.Models;
    using DaraZrt.Repository;

    /// <summary>
    /// Dara Main Logic. Maybe I will never use.
    /// </summary>
    public class DaraLogic
    {
        private readonly DaraOffice daraOffice;
        private readonly DaraStatistics daraStatistics;

        /// <summary>
        /// Initializes a new instance of the <see cref="DaraLogic"/> class.
        /// </summary>
        /// <param name="bookingrepository">Booking Repository.</param>
        /// <param name="personsrepository">Presons Repository.</param>
        /// <param name="busesrepository">Buses Repository.</param>
        /// <param name="travelrepository">Travel Repository.</param>
        /// <param name="lodgingsrepository">Lodgings Repository.</param>
        public DaraLogic(IRepository<Bookings> bookingrepository, IRepository<Persons> personsrepository, IRepository<Buses> busesrepository, IRepository<Travel> travelrepository, IRepository<Lodgings> lodgingsrepository)
        {
            this.daraOffice = new DaraOffice(bookingrepository, personsrepository, busesrepository, travelrepository, lodgingsrepository);
            this.daraStatistics = new DaraStatistics(bookingrepository, personsrepository, busesrepository, travelrepository, lodgingsrepository);
        }

        /// <summary>
        /// Gets daraOffice Getter.
        /// </summary>
        /// <returns>DaraOffice.</returns>
        public DaraOffice DaraOffice => this.daraOffice;

        /// <summary>
        /// Gets dara Statistics Getter.
        /// </summary>
        /// <returns> DaraStatistics class.</returns>
        public DaraStatistics DaraStatistics => this.daraStatistics;
    }
}
