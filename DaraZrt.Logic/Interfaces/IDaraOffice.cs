﻿// <copyright file="IDaraOffice.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic
{
    using System.Collections.Generic;
    using DaraZrt.Data.Models;
    using DaraZrt.Logic.Models;

    /// <summary>
    /// Iroda interface.
    /// </summary>
    public interface IDaraOffice
    {
        /// <summary>
        /// GetAllBuses.
        /// </summary>
        /// <returns>Inditott buszok.</returns>
        ICollection<Buses> GetAllBuses();

        /// <summary>
        /// GetAllLodgins.
        /// </summary>
        /// <returns>Lodgings.</returns>
        ICollection<Lodgings> GetAllLodgins();

        /// <summary>
        /// GetAllUtas.
        /// </summary>
        /// <returns>Persons.</returns>
        ICollection<Persons> GetAllPerson();

        /// <summary>
        /// GetAllTravel.
        /// </summary>
        /// <returns>Utazasok.</returns>
        ICollection<Travel> GetAllTravel();

        /// <summary>
        /// GeAllBookings.
        /// </summary>
        /// <returns>Foglalasok.</returns>
        ICollection<Bookings> GetAllBookings();

        /// <summary>
        /// UpdateTravels.
        /// </summary>
        /// <param name="entity">Travel entity.</param>
        void UpdateTravels(Travel entity);

        /// <summary>
        /// UpdateLodgings.
        /// </summary>
        /// <param name="entity">Szallas entity.</param>
        void UpdateLodgings(Lodgings entity);

        /// <summary>
        /// UpdateBuses.
        /// </summary>
        /// <param name="entity">Buses entity.</param>
        void UpdateBuses(Buses entity);

        /// <summary>
        /// UpdateBooking.
        /// </summary>
        /// <param name="entity">Bookings entity.</param>
        void UpdateBooking(Bookings entity);

        /// <summary>
        /// AddPersons.
        /// </summary>
        /// <param name="entity">Utas entity.</param>
        /// <returns>Id.</returns>
        int AddPersons(Persons entity);

        /// <summary>
        /// UpdatePersons.
        /// </summary>
        /// <param name="entity">Utas entity..</param>
        void UpdatePersons(Persons entity);

        /// <summary>
        /// DeleteTravel.
        /// </summary>
        /// <param name="entity">Travel entity..</param>
        void DeleteTravel(Travel entity);

        /// <summary>
        /// DeleteLodgings.
        /// </summary>
        /// <param name="entity">Szallas entity.</param>
        void DeleteLodgings(Lodgings entity);

        /// <summary>
        /// DeleteBuses.
        /// </summary>
        /// <param name="entity">(Busses entity.</param>
        void DeleteBuses(Buses entity);

        /// <summary>
        /// DeleteUtfoglalas.
        /// </summary>
        /// <param name="entity">Bookings entity.</param>
        void DeleteBooking(Bookings entity);

        /// <summary>
        /// DeletePersons.
        /// </summary>
        /// <param name="entity">Utas entity.</param>
        void DeletePersons(Persons entity);

        /// <summary>
        /// Get One Person By id.
        /// </summary>
        /// <param name="id">Person Id.</param>
        /// <returns>Person.</returns>
        Persons GetPerson(int id);

        /// <summary>
        /// Get One Bus By ID.
        /// </summary>
        /// <param name="id">Bus ID.</param>
        /// <returns>Bus.</returns>
        Buses GetBus(int id);

        /// <summary>
        /// Get Lodging by id.
        /// </summary>
        /// <param name="id">Lodgin id.</param>
        /// <returns>Lodging.</returns>
        Lodgings GetLodging(int id);

        /// <summary>
        /// Booking By Id.
        /// </summary>
        /// <param name="id">Booking Id.</param>
        /// <returns>Booking.</returns>
        Bookings GetBooking(int id);

        /// <summary>
        /// Travel By id.
        /// </summary>
        /// <param name="id">Travel id.</param>
        /// <returns>Travel.</returns>
        Travel GetTravel(int id);
    }
}
