﻿// <copyright file="IDaraStatistics.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DaraZrt.Logic.Models;

    /// <summary>
    ///  Dara zrt. Statistic interface.
    /// </summary>
    public interface IDaraStatistics
    {
        /// <summary>
        /// GetCheapTravels.
        /// </summary>
        /// <returns>Olcso utazasok.</returns>
        ICollection<CheapBooking> GetCheapBooking();

        /// <summary>
        /// GetBookableBusses.
        /// </summary>
        /// <returns>Foglalhato Buszok.</returns>
        ICollection<BookableBusses> GetBookableBusses();

        /// <summary>
        /// nyari dubaij.
        /// </summary>
        /// <returns>Nyari dubaij.</returns>
        ICollection<SummerPeople> GetSummerPeople();

        /// <summary>
        /// Cheap Booking Async.
        /// </summary>
        /// <returns> Task.</returns>
        Task<ICollection<CheapBooking>> GetCheapBookingAsync();
    }
}
