﻿// <copyright file="DaraStatistics.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DaraZrt.Data.Models;
    using DaraZrt.Logic.Models;
    using DaraZrt.Repository;

    /// <summary>
    /// Dara Statistics.
    /// </summary>
    public class DaraStatistics : IDaraStatistics
    {
        private readonly IRepository<Bookings> bookingsrepository;
        private readonly IRepository<Persons> personsrepository;
        private readonly IRepository<Buses> busesrepository;
        private readonly IRepository<Travel> travelrepository;
        private readonly IRepository<Lodgings> lodgingsrepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="DaraStatistics"/> class.
        /// </summary>
        /// <param name="bookingrepository">Booking repository.</param>
        /// <param name="personsrepository">Persons Repository.</param>
        /// <param name="busesrepository">Bueses Repository.</param>
        /// <param name="travelrepository">Travel Repository.</param>
        /// <param name="lodgingsrepository">Lodgings Repository.</param>
        public DaraStatistics(IRepository<Bookings> bookingrepository, IRepository<Persons> personsrepository, IRepository<Buses> busesrepository, IRepository<Travel> travelrepository, IRepository<Lodgings> lodgingsrepository)
        {
            this.bookingsrepository = bookingrepository;
            this.personsrepository = personsrepository;
            this.busesrepository = busesrepository;
            this.travelrepository = travelrepository;
            this.lodgingsrepository = lodgingsrepository;
        }

        /// <summary>
        /// GetCheapTravels.
        /// </summary>
        /// <returns>Azok az utazasok, melyeknek az ara olcsobb mint 100000.</returns>
        public ICollection<CheapBooking> GetCheapBooking()
        {
            var q1 = from booking in this.bookingsrepository.GetAll()
                     join lodging in this.lodgingsrepository.GetAll() on booking.LodgingsId equals lodging.Id
                     select new
                     {
                         booking.Id,
                         booking.TravelId,
                         LodgingPrice = lodging.Price,
                         lodging.Hotel,
                         lodging.Quality,
                     };
            var q2 = from booking in q1
                     join travel in this.travelrepository.GetAll() on booking.TravelId equals travel.Id
                     select new CheapBooking
                     {
                         Id = booking.Id,
                         Price = travel.Price + booking.LodgingPrice,
                         Ends = travel.Ends,
                         Starts = travel.Starts,
                         Country = travel.Country,
                         City = travel.City,
                         Hotel = booking.Hotel,
                         Quality = booking.Quality,
                     };

            return q2.Where(x => x.Price < 50000).ToList();
        }

        /// <summary>
        /// GetBookableBusses.
        /// </summary>
        /// <returns>Bueses Where there is Free space.</returns>
        public ICollection<BookableBusses> GetBookableBusses()
        {
            /* Select buszID, count(*) as foglalt  from utfoglalas WHERE ulohely is not null group by  buszID */

            var q1 = from foglalas in this.bookingsrepository.GetAll().Where(x => x.Seats != null)
                     group foglalas by foglalas.BusId into foglalGroup
                     select new
                     {
                         id = foglalGroup.Key,
                         foglaltUlohely = foglalGroup.Count(),
                     };
            var q2 = from busz in this.busesrepository.GetAll()
                     join foglaltbusz in q1 on busz.Id equals foglaltbusz.id
                     select new BookableBusses
                     {
                         Id = busz.Id,
                         LicencePlate = busz.LicencePlate,
                         NumberOfFreeSeats = busz.Capacity - foglaltbusz.foglaltUlohely,
                         NumberOfSeats = busz.Capacity,
                     };
            return q2.Where(x => x.NumberOfFreeSeats >= 1).ToList(); // Szures a vegen
        }

        /// <summary>
        /// GetDubaiSummer.
        /// </summary>
        /// <returns>Dubai Summer Busses free steats.</returns>
        public ICollection<SummerPeople> GetSummerPeople()
        {
            var q1 = from utazas in this.travelrepository.GetAll().Where(x => x.Starts.Month >= 6 && x.Starts.Month <= 8)
                     join foglalas in this.bookingsrepository.GetAll() on utazas.Id equals foglalas.TravelId
                     select new
                     {
                         Start = utazas.Starts,
                         End = utazas.Ends,
                         foglalasId = foglalas.Id,
                         peopleId = foglalas.PersonId,
                         city= utazas.City,
                     };
            var q2 = from people in this.personsrepository.GetAll()
                     join foglal in q1 on people.Id equals foglal.peopleId
                     select new SummerPeople
                     {
                        City = foglal.city,
                        Id = people.Id,
                        End = foglal.End,
                        Start = foglal.Start,
                        FirstName = people.FirstName,
                        LastName = people.LastName,
                     };
            return q2.ToList();
        }

        /// <summary>
        /// Async GetCheapBooking.
        /// </summary>
        /// <returns> Cheap Booking List. </returns>
        public Task<ICollection<CheapBooking>> GetCheapBookingAsync()
        {
            var q1 = from booking in this.bookingsrepository.GetAll()
                     join lodging in this.lodgingsrepository.GetAll() on booking.LodgingsId equals lodging.Id
                     select new
                     {
                         booking.Id,
                         booking.TravelId,
                         LodgingPrice = lodging.Price,
                         lodging.Hotel,
                         lodging.Quality,
                     };
            var q2 = from booking in q1
                     join travel in this.travelrepository.GetAll() on booking.TravelId equals travel.Id
                     select new CheapBooking
                     {
                         Id = booking.Id,
                         Price = travel.Price + booking.LodgingPrice,
                         Ends = travel.Ends,
                         Starts = travel.Starts,
                         Country = travel.Country,
                         City = travel.City,
                         Hotel = booking.Hotel,
                         Quality = booking.Quality,
                     };

            ICollection<CheapBooking> lists = q2.Where(x => x.Price < 50000).ToList();
            return Task.FromResult(lists);
        }

        /// <summary>
        /// GetBookableBusses.
        /// </summary>
        /// <returns>Bueses Where there is Free space.</returns>
        public Task<ICollection<BookableBusses>> GetBookableBussesAsync()
        {
            /* Select buszID, count(*) as foglalt  from utfoglalas WHERE ulohely is not null group by  buszID */

            var q1 = from foglalas in this.bookingsrepository.GetAll().Where(x => x.Seats != null)
                     group foglalas by foglalas.BusId into foglalGroup
                     select new
                     {
                         id = foglalGroup.Key,
                         foglaltUlohely = foglalGroup.Count(),
                     };
            var q2 = from busz in this.busesrepository.GetAll()
                     join foglaltbusz in q1 on busz.Id equals foglaltbusz.id
                     select new BookableBusses
                     {
                         Id = busz.Id,
                         LicencePlate = busz.LicencePlate,
                         NumberOfFreeSeats = busz.Capacity - foglaltbusz.foglaltUlohely,
                         NumberOfSeats = busz.Capacity,
                     };
            ICollection<BookableBusses> list = q2.Where(x => x.NumberOfFreeSeats >= 1).ToList();
            return Task.FromResult(list);
        }

        /// <summary>
        /// GetDubaiSummer.
        /// </summary>
        /// <returns>Dubai Summer Busses free steats.</returns>
        public Task<ICollection<SummerPeople>> GetSummerPeopleAsync()
        {
            var q1 = from utazas in this.travelrepository.GetAll().Where(x => x.Starts.Month >= 6 && x.Starts.Month <= 8)
                     join foglalas in this.bookingsrepository.GetAll() on utazas.Id equals foglalas.TravelId
                     select new
                     {
                         Start = utazas.Starts,
                         End = utazas.Ends,
                         foglalasId = foglalas.Id,
                         peopleId = foglalas.PersonId,
                         city = utazas.City,
                     };
            var q2 = from people in this.personsrepository.GetAll()
                     join foglal in q1 on people.Id equals foglal.peopleId
                     select new SummerPeople
                     {
                         City = foglal.city,
                         Id = people.Id,
                         End = foglal.End,
                         Start = foglal.Start,
                         FirstName = people.FirstName,
                         LastName = people.LastName,
                     };
            ICollection<SummerPeople> list = q2.ToList();
            return Task.FromResult(list);
        }
    }
}
