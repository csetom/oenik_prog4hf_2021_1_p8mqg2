﻿// <copyright file="DaraDBContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace DaraZrt.Data
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DaraZrt.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// DaraDbContext.
    /// </summary>
    public class DaraDBContext : DbContext
{
        /// <summary>
        /// DefaultConsStr.
        /// </summary>
        private const string DefaultConnStr = @"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\DaraDB.mdf;Integrated Security = True;MultipleActiveResultSets=True";

        /// <summary>
        /// Initializes a new instance of the <see cref="DaraDBContext"/> class.
        /// </summary>
        public DaraDBContext()
            : base()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DaraDBContext"/> class.
        /// </summary>
        /// <param name="options"> Ezek a beallitasok. </param>
        public DaraDBContext(DbContextOptions<DaraDBContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets inditott buszok.
        /// </summary>
        public DbSet<Buses> Busses { get; set; }

        /// <summary>
        ///  Gets or sets.
        /// </summary>
        public DbSet<Lodgings> Lodgings { get; set; }

        /// <summary>
        ///  Gets or sets.
        /// </summary>
        public DbSet<Persons> Persons { get; set; }

        /// <summary>
        ///  Gets or sets.
        /// </summary>
        public DbSet<Travel> Travel { get; set; }

        /// <summary>
        ///  Gets or sets.
        /// </summary>
        public DbSet<Bookings> Bookings { get; set; }

        /// <summary>
        /// OnConfiguring.
        /// </summary>
        /// <param name="optionsBuilder"> Configuracios beallitasok. </param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder is null)
            {
                throw new ArgumentNullException(nameof(optionsBuilder));
            }

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(DefaultConnStr);
            }
        }

        /// <summary>
        /// Model Creating.
        /// Adatok feltoltese.
        /// </summary>
        /// <param name="modelBuilder"> Modelbuilder maga.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder is null)
            {
                throw new ArgumentNullException(nameof(modelBuilder));
            }

            modelBuilder.Entity<Bookings>(foglalas =>
            {
                foglalas.HasOne(f => f.Lodgings).WithMany(x => x.Bookings);
                foglalas.HasOne(f => f.Persons).WithMany(x => x.Bookings);
                foglalas.HasOne(f => f.Buses).WithMany(x => x.Bookings);
                foglalas.HasOne(f => f.Travel).WithMany(x => x.Bookings);
            });
            Buses busz1 = new Buses() { Id = 1, Capacity = 50, LicencePlate = "ABC-123", Departures = DateTime.ParseExact("2023.06.08T08:10", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2023.06.09T21:33", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 5 };
            Buses busz2 = new Buses() { Id = 2, Capacity = 20, LicencePlate = "GEG-001", Departures = DateTime.ParseExact("2022.09.08T10:11", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2022.09.09T10:00",  "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 7 };
            Buses busz3 = new Buses() { Id = 3, Capacity = 100, LicencePlate = "FIN-626", Departures = DateTime.ParseExact("2021.12.01T05:05", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Arrival = DateTime.ParseExact("2021.12.01T22:09",  "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), StopNumber = 9 };
            Lodgings szallas1 = new Lodgings() { Id = 1, Price = 20000, Quality = "Alap", Hotel = "Furaszalloda", RoomNumber = "G2", Space = 2, };
            Lodgings szallas2 = new Lodgings() { Id = 2, Price = 40000, Quality = "Minosegi", Hotel = "Furaszalloda", RoomNumber = "G3", Space = 2, };
            Lodgings szallas3 = new Lodgings() { Id = 3, Price = 60000, Quality = "Kiralyi", Hotel = "Furaszalloda", RoomNumber = "G4", Space = 2, };
            Lodgings szallas4 = new Lodgings() { Id = 4, Price = 10000, Quality = "Alap", Hotel = "Kecske lábszár", RoomNumber = "A1", Space = 3, };
            Lodgings szallas5 = new Lodgings() { Id = 5, Price = 20000, Quality = "Minosegi", Hotel = "Kecske lábszár", RoomNumber = "A2", Space = 1, };
            Lodgings szallas6 = new Lodgings() { Id = 6, Price = 30000, Quality = "Alap", Hotel = "Lépcsős Pincelakók", RoomNumber = "A113", Space = 6, };
            Persons utas1 = new Persons() { Id = 1, Email = "Karcsi@Karcsilagzi.hu", FirstName = "Károly", LastName = "Buli", PhoneNumber = "+363033333333", Adress = "1111 Budapest XI. Balaton utca 2" };
            Persons utas2 = new Persons() { Id = 2, Email = "Marika@mzperx.hu", FirstName = "Mariann", LastName = "Soos-Ferenczné Nagy ", PhoneNumber = "+320000351000", Adress = "8174 Balatonkenese Simon István utca 11" };
            Persons utas3 = new Persons() { Id = 3, Email = "Zoldlevelibeka@narancsmail.hu", FirstName = "Gergely", LastName = "Csíkos", PhoneNumber = "+310011110", Adress = "8172 Balatonakarattya  Zöld-ház út 222" };
            Travel utazas1 = new Travel() { Id = 1, Price = 20000, Starts = DateTime.ParseExact("2023.06.09", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2023.06.16", "yyyy.MM.dd", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" };
            Travel utazas2 = new Travel() { Id = 2, Price = 300000, Starts = DateTime.ParseExact("2022.09.02T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Ends = DateTime.ParseExact("2022.09.09T10:00", "yyyy.MM.ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture), Country = "Egyesült Arab Emírségek", City = "Dubaj" };

            // Bookings foglal1 = new Bookings() { Id = 1, BusId = busz1.Id, PersonId = utas2.Id, Date = DateTime.Now, TravelId = utazas1.Id, LodgingsId = szallas1.Id, Seats = "42" };
            // Bookings foglal2 = new Bookings() { Id = 2, BusId = busz2.Id, PersonId = utas2.Id, Date = DateTime.Now, TravelId = utazas2.Id, LodgingsId = szallas5.Id, Seats = "22" };
            // Bookings foglal3 = new Bookings() { Id = 3, BusId = busz3.Id, PersonId = utas1.Id, Date = DateTime.Now, TravelId = utazas1.Id, LodgingsId = szallas2.Id, Seats = "39" };
            // Bookings foglal4 = new Bookings() { Id = 4, BusId = busz3.Id, PersonId = utas3.Id, Date = DateTime.Now, TravelId = utazas2.Id, LodgingsId = szallas6.Id, Seats = "11" };
            modelBuilder.Entity<Buses>().HasData(busz1, busz2, busz3);
            modelBuilder.Entity<Lodgings>().HasData(szallas1, szallas2, szallas3, szallas4, szallas5, szallas6);
            modelBuilder.Entity<Persons>().HasData(utas1, utas2, utas3);
            modelBuilder.Entity<Travel>().HasData(utazas1, utazas2);

            // modelBuilder.Entity<Bookings>().HasData(foglal1, foglal2, foglal3, foglal4);
        }
    }
}
