﻿// <copyright file="Buses.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Busses.
    /// </summary>
    [Table("Buses")]
    public class Buses
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Buses"/> class.
        /// </summary>
        public Buses()
        {
            this.Bookings = new HashSet<Bookings>();
        }

        /// <summary>
        ///  Gets or sets id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets LicencePlate.
        /// </summary>
        public string LicencePlate { get; set; }

        /// <summary>
        /// Gets or sets StopNumber.
        /// </summary>
        public int StopNumber { get; set; }

        /// <summary>
        /// Gets or sets Capacity.
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// Gets or sets Departures.
        /// </summary>
        public DateTime Departures { get; set; }

        /// <summary>
        /// Gets or sets Arrival.
        /// </summary>
        public DateTime Arrival { get; set; }

        /// <summary>
        /// Gets bookings.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Bookings> Bookings { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $" {this.Id}, {this.LicencePlate}, {this.StopNumber}, {this.Capacity}, {this.Arrival}, {this.Departures} ";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Buses)
            {
                Buses other = obj as Buses;
                return this.Id == other.Id &&
                        this.Arrival == other.Arrival &&
                        this.Departures == other.Departures &&
                        this.Capacity == other.Capacity &&
                        this.LicencePlate == other.LicencePlate &&
                        this.StopNumber == other.StopNumber;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Id;
        }
    }
}
