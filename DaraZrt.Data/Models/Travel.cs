﻿// <copyright file="Travel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Travel.
    /// </summary>
    [Table("Travel")]
    public class Travel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Travel"/> class.
        ///  Travel.
        /// </summary>
        public Travel()
        {
            this.Bookings = new HashSet<Bookings>();
        }

        /// <summary>
        /// Gets or sets id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets mettol.
        /// </summary>
        public DateTime Starts { get; set; }

        /// <summary>
        /// Gets or sets meddig.
        /// </summary>
        public DateTime Ends { get; set; }

        /// <summary>
        /// Gets or sets orszag.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets varos.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets ar.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Gets foglalasok.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Bookings> Bookings { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $" {this.Id}, {this.Price}, {this.City}, {this.Country}, {this.Starts}, {this.Ends}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Travel)
            {
                Travel other = obj as Travel;
                return this.Id == other.Id &&
                        this.City == other.City &&
                        this.Country == other.Country &&
                        this.Starts == other.Starts &&
                        this.Ends == other.Ends &&
                        this.Price == other.Price;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Id;
        }
    }
}
