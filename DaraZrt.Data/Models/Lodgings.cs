﻿// <copyright file="Lodgings.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Szallas Table.
    /// </summary>
    [Table("Lodgings")]
    public class Lodgings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Lodgings"/> class.
        /// </summary>
        public Lodgings()
        {
            this.Bookings = new HashSet<Bookings>();
        }

        /// <summary>
        /// Gets or sets id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets szobaszam.
        /// </summary>
        public string RoomNumber { get; set; }

       /// <summary>
       /// Gets or sets szalloda.
       /// </summary>
        public string Hotel { get; set; }

        /// <summary>
        /// Gets or sets ar.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets the Space of the room.
        /// </summary>
        public int Space { get; set; }

        /// <summary>
        /// gets or sets szobaminosege.
        /// </summary>
        public string Quality { get; set; }

        /// <summary>
        /// Gets utfoglalasok.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Bookings> Bookings { get; }
    }
}
