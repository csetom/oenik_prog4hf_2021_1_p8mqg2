﻿// <copyright file="Bookings.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Bookings.
    /// </summary>
    [Table("Bookings")]
    public class Bookings
    {
        /// <summary>
        /// Gets or sets id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets busId.
        /// </summary>
        [ForeignKey(nameof(Buses))]
        public int BusId { get; set; }

        /// <summary>
        /// Gets or sets utasId.
        /// </summary>
        [ForeignKey(nameof(Persons))]
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets ulohely.
        /// </summary>
        public string Seats { get; set; }

        /// <summary>
        ///  Gets or sets datum.
        /// </summary>
        public DateTime Date { get; set; } // Foglalasi datum

        /// <summary>
        ///  Gets or sets szallasid.
        /// </summary>
        [ForeignKey(nameof(Lodgings))]
        public int LodgingsId { get; set; }

        /// <summary>
        ///  Gets or sets utazasid.
        /// </summary>
        [ForeignKey(nameof(Travel))]
        public int TravelId { get; set; }

        /// <summary>
        ///  Gets or sets utazas.
        /// </summary>
        [NotMapped]
        public virtual Travel Travel { get; set; }

        /// <summary>
        ///  Gets or sets utas.
        /// </summary>
        [NotMapped]
        public virtual Persons Persons { get; set; }

        /// <summary>
        ///  Gets or sets inditottbuszok.
        /// </summary>
        [NotMapped]
        public virtual Buses Buses { get; set; }

        /// <summary>
        ///  Gets or sets szallas.
        /// </summary>
        [NotMapped]
        public virtual Lodgings Lodgings { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Bookings)
            {
                Bookings other = obj as Bookings;
                return this.Id == other.Id &&
                        this.BusId == other.BusId &&
                        this.LodgingsId == other.LodgingsId &&
                        this.PersonId == other.PersonId &&
                        this.TravelId == other.TravelId &&
                        this.Seats == other.Seats;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Id;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.Id},{this.BusId}, {this.LodgingsId}, {this.PersonId}, {this.TravelId}, {this.Seats},";
        }
    }
}
