﻿// <copyright file="Persons.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DaraZrt.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Dynamic;
    using System.Text;

    /// <summary>
    /// Persons.
    /// </summary>
    [Table("Persons")]
    public class Persons
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Persons"/> class.
        /// Utas.
        /// </summary>
        public Persons()
        {
            this.Bookings = new HashSet<Bookings>();
        }

        /// <summary>
        /// Gets or sets id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets keresztnev.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets vezeteknev.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets lakcim.
        /// </summary>
        public string Adress { get; set; }

        /// <summary>
        /// Gets or sets telefon.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets foglalasok.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Bookings> Bookings { get; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Persons)
            {
                Persons other = obj as Persons;
                return this.Id == other.Id &&
                        this.FirstName == other.FirstName &&
                        this.LastName == other.LastName &&
                        this.Email == other.Email &&
                        this.PhoneNumber == other.PhoneNumber &&
                        this.Adress == other.Adress;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Id;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.Id},{this.FirstName},{this.LastName},{this.Email},{this.PhoneNumber},{this.Adress}";
        }
    }
}
